"""
Pyeigfeast submodule of ready-made scripts and utilities
"""
__all__ = ['exact_eig_unitsqr', 'print_apparent_rates',
           'print_rates', 'print_ew_rates',
           'eig_dpg_feast_dir', 'inputs_for_dpg',
           'eig_fem_feast', 'direct_eig', 'eig_dpg_feast_dir_unitsqr',
           'run_feast_1dtridiag', 'eig_dpg_feast_schrodinger']

from .utils import exact_eig_unitsqr
from .utils import print_apparent_rates, print_rates, print_ew_rates
from .dirichletDPG import eig_dpg_feast_dir, inputs_for_dpg
from .dirichletDPG import eig_dpg_feast_dir_unitsqr
from .dirichletFEM import eig_fem_feast, direct_eig
from .dirichlet1D import run_feast_1dtridiag
from .schrodinger import eig_dpg_feast_schrodinger
