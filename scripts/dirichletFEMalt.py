"""
Simple script solving the Poisson eigenproblem to illustrate
the use of classes SpectralProjNGR, ResolventAB.
"""

from pyeigfeast.spectralproj.ngs import SpectralProjNGR, ResolventAB, NGvecs
from netgen.geom2d import unit_square
from ngsolve import grad, dx
import ngsolve as ng


h = 0.1      # mesh size for finite element discretization
p = 2        # degree of Lagrange finite elements to use
m = 4        # number of vectors in initial eigenspace iterate
ctr = 19.0   # contour center
rad = 40     # contour radius
npts = 8     # number of points in trapezoidal rule

ngmesh = unit_square.GenerateMesh(maxh=h)
mesh = ng.Mesh(ngmesh)
for i in range(len(mesh.GetBoundaries())):
    # mark Dirichlet bc parts as 'allbdry'
    ngmesh.SetBCName(i, 'allbdry')
X = ng.H1(mesh, order=p, dirichlet='allbdry', complex=True)
u, v = X.TnT()
a = ng.BilinearForm(X)
a += grad(u) * grad(v) * dx
b = ng.BilinearForm(X)
b += u * v * dx
with ng.TaskManager():
    a.Assemble()
    b.Assemble()

P = SpectralProjNGR(lambda z: ResolventAB(X, a.mat, b.mat, z),
                    reduce_sym=True, radius=rad, center=ctr, npts=npts)

Y = NGvecs(X, m, b.mat)
Y.setrandom()

lam, Y, history, _ = P.feast(Y)
