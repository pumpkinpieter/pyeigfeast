"""Study of FEAST iteration speeds (speed of convergence to the
discrete eigenspace) for the first 3 Dirichlet eigenvalues on the unit
square

"""

from dirichletDPG import eig_dpg_feast_dir_unitsqr
import numpy as np
import matplotlib.pyplot as plt

p = 1          # polynomial degree of DPG discretization
h = 2**(-5)    # meshsize

m = 4          # number of vectors in initial eigenspace iterate
ctr = 10.0     # contour center
rad = 40       # contour radius
ewn = 3        # number of exact eigenvalues within the contour
N = 20         # number of points in trapezoidal rule


print("Computing ... case of p=%d, h=%g" % (p, h))
lam, Y, history, P = eig_dpg_feast_dir_unitsqr(h, p, m, ctr, rad, N,
                                               verbose=True)
ewerrors, eferrors, ewchanges, residuals, ewhistory, cgd = history
if len(lam) != ewn:
    print('\n***FEAST did not get the right eigenspace dimension!')
    print('   Try again with better h, p, N, or thresholds.\n')
    import sys
    sys.exit(1)

efe = np.array(eferrors)
efd = efe[:-1, :] - efe[1:, :]

print(efd)

itern = np.arange(efd.shape[0])
plt.semilogy(abs(efd[:, 0]), '-*r', label='1st ef')
plt.semilogy(abs(efd[:, 1]), '-+b', label='2nd ef')
plt.semilogy(abs(efd[:, 2]), '-og', label='3rd ef')
plt.grid(True)
plt.legend()
plt.show()
