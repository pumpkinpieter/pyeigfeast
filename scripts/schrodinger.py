"""Approximate a part of the spectrum of the Schrodinger operator

A u = -Delta u + V u

with Dirichlet bc at the boundary of the square [-d,d] x [-d,d]. As d
becomes large and V represents a negative potential well, we expect to
get bound states as well as components that approximate the essential
spectrum. Type 'netgen schrodinger.py' to visualize the computed
Schrodinger modes.

"""

from pyeigfeast.spectralproj.dpg import NGvecs, SpectralProjDPG
from netgen.geom2d import SplineGeometry
import ngsolve as ngs
from ngsolve import x, y, exp
import numpy as np


def inputs_for_dpg_schrodinger(mesh, p, V):
    """
    Things for setting up DPG Spectral Projector for the approximation
    of the Schrodinger eigenproblem using finite element spaces
    determined by input degree "p". Potential is input in "V".
    """

    # Finite element spaces for the DPG method:

    U = ngs.H1(mesh, order=p, dirichlet="allbdry", complex=True)
    Q = ngs.HDiv(mesh, order=p-1, orderinner=0, complex=True)
    Y = ngs.L2(mesh, order=p+1, complex=True)
    component_spaces = [U, Q, Y]
    X = ngs.FESpace(component_spaces, complex=True)
    uindex = 0
    yindex = 2

    # DPG method uses b( (u,q),d ) = (grad u,grad d) + (Vu, d) + <q.n,d>.
    # We need to form   -b((u,q),d) - conjugate( b((w,r),e) ):

    u, q, e = X.TrialFunction()
    w, r, d = X.TestFunction()
    n = ngs.specialcf.normal(mesh.dim)
    gradu = u.Deriv()
    gradw = w.Deriv()
    grade = e.Deriv()
    gradd = d.Deriv()

    # Form   -b((u,q),d) - conjugate( b((w,r),e) ):

    minus_bb = []
    minus_bb += [ngs.SymbolicBFI(-gradu * gradd - grade * gradw)]
    minus_bb += [ngs.SymbolicBFI(-V * u * d - e * ngs.Conj(V) * w)]
    minus_bb += [ngs.SymbolicBFI(-q*n*d, element_boundary=True)]
    minus_bb += [ngs.SymbolicBFI(-e*r*n, element_boundary=True)]

    # Form  Y inner product:

    y_integrators = [ngs.SymbolicBFI(grade * gradd + e * d)]

    # Make Laplace operator A and mass M on U:

    u = U.TrialFunction()
    w = U.TestFunction()
    gradu = u.Deriv()
    gradw = w.Deriv()
    mss = ngs.BilinearForm(U, symmetric=True)
    mss += ngs.SymbolicBFI(u * w)
    mss.Assemble()
    a = ngs.BilinearForm(U)
    a += ngs.SymbolicBFI(gradu * gradw + V * u * w)
    a.Assemble()

    return (minus_bb, y_integrators, a.mat, mss.mat,
            X, component_spaces, uindex, yindex, mesh)


def eig_dpg_feast_schrodinger(h, d, p, V, m, ctr, rad, npts,
                              verbose=True, quadrule='circ_trapez_shift',
                              check_contour=3, niterations=100):
    """
    Apply FEAST with DPG discretization for the Schrodinger
    eigenproblem.  INPUTS:
    h = max element diameter
    d = computational domain set to [-d,d]^2
    V = potential for the Schrodinger eq as an ngspy coefficient
    (See dirichletDPG.py for documentation on the other input/outputs.)
    """

    ngs.ngsglobals.msg_level = 1

    # Mesh:

    geometry = SplineGeometry()
    pntlist = [(-d, -d), (d, -d), (d, d), (-d, d)]
    pnts = [geometry.AppendPoint(*p) for p in pntlist]
    seglist = [(pnts[0], pnts[1], 'allbdry', 1, 0),
               (pnts[1], pnts[2], 'allbdry', 1, 0),
               (pnts[2], pnts[3], 'allbdry', 1, 0),
               (pnts[3], pnts[0], 'allbdry', 1, 0)]
    for p0, p1, bn, ml, mr in seglist:
        geometry.Append(['line', p0, p1],
                        bc=bn, leftdomain=ml, rightdomain=mr)
    ngm = geometry.GenerateMesh(maxh=h)
    mesh = ngs.Mesh(ngm)
    for i, bdries in enumerate(mesh.GetBoundaries()):
        ngm.SetBCName(i, 'allbdry')

    # Set up DPG forms for Schrodinger eigenproblem

    minus_bb_integrators, y_integrators,                      \
        Amat, Mmat, X, component_spaces, uindex, yindex, mesh \
        = inputs_for_dpg_schrodinger(mesh, p, V)

    # Make spectral projection approximation using DPG:

    U = component_spaces[uindex]
    P = SpectralProjDPG(minus_bb_integrators, y_integrators,
                        Amat, Mmat,
                        X, component_spaces, uindex, yindex, mesh,
                        rad, ctr, npts,
                        quadrule=quadrule, verbose=verbose, reduce_sym=True)
    Y = NGvecs(U, m, Mmat, verbose=verbose)
    Y.setrandom()

    # Run FEAST algorithm

    lam, Y, history, _ = P.feast(Y, cut_tol=1.e-6, check_contour=check_contour,
                                 reportresiduals=True, niterations=niterations)
    ngs.Draw(V, mesh, 'potential')
    Y.draw()

    return (lam, Y, mesh, history, P)


def save_realpart_vtk(filename, mesh, lam, Y, vcf, p, scale=1):
    """
    Save the real part of the computed eigen basis Y into vtk format
    for visualization, together with the potential V. Note that Y
    vectors may change sign after this function is called.
    """

    realU = ngs.H1(mesh, order=p, dirichlet="allbdry")
    realY = []
    names = []
    for i in range(Y.m):

        # Initialize g to constant function equaling height of eigenvalue
        g = ngs.GridFunction(realU)
        g.Set(ngs.CoefficientFunction(lam[i]/scale))

        # Put Y[i] into a grid function gY
        gY = ngs.GridFunction(realU)
        for j in range(len(Y._mv[i])):
            gY.vec[j] = Y._mv[i][j].real

        # Change the sign of gY depending on its value at origin
        g0 = gY(mesh(0, 0))
        if g0 < 0:
            gY.vec.data = -1.0 * gY.vec
            Y._mv[i].data = -1.0 * Y._mv[i]

        # Append g = gY + eigenvalue height to visualization
        for j in range(len(Y._mv[i])):
            g.vec[j] = gY.vec[j] + g.vec[j]
        realY.append(g)
        names.append('Y%d' % i)

    names.append('potential')
    realY.append((1.0/scale)*vcf)
    vtk = ngs.VTKOutput(ma=mesh, coefs=realY, names=names,
                        filename=filename, subdivision=p)
    vtk.Do()


if __name__ == '__main__':

    # Set your favorite potential function here:

    V = -50*exp(-(x*x+y*y))

    # To see bound states try this:
    m = 10        # number of vectors in initial eigenspace iterate
    ctr = -40.0   # contour center
    rad = 30      # contour radius
    # # To see what may be parts of essential spectrum, try this:
    # m = 35        # number of vectors in initial eigenspace iterate
    # ctr = 0.0     # contour center
    # rad = 10      # contour radius

    d = 5         # computational domain [-d,d] x [-d,d]
    h = 0.4       # max element diameter
    p = 2         # polynomial degree of approximation space
    npts = 8      # number of points in trapezoidal rule

    vcf = ngs.CoefficientFunction(V)
    lam, Y, mesh, history, \
        P = eig_dpg_feast_schrodinger(h, d, p, vcf, m,
                                      ctr, rad, npts,
                                      quadrule='circ_trapez_shift',
                                      niterations=100)
    ewerrors, eferrors, ewhdist, residuals, ews, converged = history
    np.savez('outputs/tmp_schrodinger_cgce.npz',
             residual=residuals, lam=lam,
             h=h, p=p, d=d, npts=npts)
    save_realpart_vtk('outputs/tmp_schrodinger', mesh, lam, Y, vcf, p, scale=5)
