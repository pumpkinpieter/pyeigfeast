"""

Compute first few eigenvalues of a dumbbell-shaped domain using feast/dpg.

"""

from dirichletDPG import eig_dpg_feast_dir
from ratesDPGsqr import print_apparent_rates
from netgen.geom2d import SplineGeometry
import ngsolve as ngs
import numpy as np


def make_dumbbell(W, h):
    """
    Make the geometry of a dumbbell-shaped domain consisting of two unit
    squares connected by a centered WxW square tunnel. Return a mesh
    of the domain with element diameter not exceeding h.
    """

    if (W > 1):
        raise ValueError('Connecting channel height W should be < 1!')

    geometry = SplineGeometry()
    pntlist = [(0, 0), (1, 0), (1, 1), (0, 1),
               (1, 0.5*(1-W)), (1, 0.5*(1+W)),
               (1+W, 0.5*(1-W)), (1+W, 0.5*(1+W)),
               (1+W, 0), (2+W, 0), (2+W, 1), (1+W, 1)]
    pnts = [geometry.AppendPoint(*p) for p in pntlist]
    seglist = [(pnts[5], pnts[2], 'allbdry', 1, 0),
               (pnts[2], pnts[3], 'allbdry', 1, 0),
               (pnts[3], pnts[0], 'allbdry', 1, 0),
               (pnts[0], pnts[1], 'allbdry', 1, 0),
               (pnts[1], pnts[4], 'allbdry', 1, 0),
               (pnts[4], pnts[6], 'allbdry', 1, 0),
               (pnts[6], pnts[8], 'allbdry', 1, 0),
               (pnts[8], pnts[9], 'allbdry', 1, 0),
               (pnts[9], pnts[10], 'allbdry', 1, 0),
               (pnts[10], pnts[11], 'allbdry', 1, 0),
               (pnts[11], pnts[7], 'allbdry', 1, 0),
               (pnts[7], pnts[5], 'allbdry', 1, 0)]
    for p0, p1, bn, ml, mr in seglist:
        geometry.Append(['line', p0, p1],
                        bc=bn, leftdomain=ml, rightdomain=mr)
    m = geometry.GenerateMesh(maxh=h)
    return m


def run_feast_dumbbell(W, h, p, m, ctr, rad, npts, stop_tol=1.e-9,
                       quadrule='circ_trapez_shift', rhoinv=0.0):

    ngs.ngsglobals.msg_level = 0
    mesh = ngs.Mesh(make_dumbbell(W, h))
    return eig_dpg_feast_dir(mesh, p, m, ctr, rad, npts,
                             stop_tol=stop_tol, exactsol=None,
                             quadrule=quadrule, rhoinv=rhoinv)


def convergence_rates(W, maxr, p, m, ctr, rad, npts, h0=0.25,
                      quadrule='circ_trapez_shift', rhoinv=0.0,
                      outputfile='outputs/tmpdumbbell.npz'):
    """
    Perform a sequence of mesh refinements starting from coarse mesh of
    element size h0, run feast, and estimate apparent convergence rates.
    """

    ngs.ngsglobals.msg_level = 0
    ngm0 = make_dumbbell(W, h0)
    mesh = ngs.Mesh(ngm0)

    ews = []
    hs = []
    res = []

    for ll in range(maxr):

        h = h0 * 2**(-ll)
        hs.append(h)
        print('Case h=%g: Computing ...' % h)

        lam, Y, history, P = eig_dpg_feast_dir(mesh, p, m, ctr, rad, npts,
                                               exactsol=None, verbose=False,
                                               quadrule=quadrule,
                                               rhoinv=rhoinv)
        ewerrors, eferrors, ewhdist, residuals, ews, converged = history
        ews.append(lam)
        res.append(residuals[-1])
        mesh.Refine()

    ew = np.array(ews)
    print_apparent_rates(ew, hs)
    np.savez(outputfile, ews=ews, hs=hs, res=res)
    return (ews, hs, res)


def collect_rates_from_file(npzfilename):

    npf = np.load(npzfilename)
    ews = npf['ews']
    hs = npf['hs']
    # res = npf['res']
    print_apparent_rates(ews, hs)


if __name__ == '__main__':

    # PARAMETERS:

    W = 0.25     # dumbbell tunnel width
    m = 10       # number of vectors in initial eigenspace iterate
    ctr = 20.0   # contour center
    rad = 40     # contour radius
    npts = 4     # number of points in trapezoidal rule
    p = 2        # polynomial degree of DPG discretization
    maxr = 6     # max num refinements (only for convergence study)
    h0 = 0.25    # coarsest mesh size (only for convergence study)
    tol = 1e-9   # stop when ew changes by less than tol

    # convergence_rates(W, maxr, p, m, ctr, rad, npts, h0=0.25)

    # convergence_rates(W, maxr, p, m, ctr, rad, npts, h0=0.25,
    #                  quadrule='ellipse_trapez_shift', rhoinv=0.4)

    # run_feast_dumbbell(W, h0/4, p, m, ctr, rad, npts, stop_tol=tol)

    lam, Y, history, P = run_feast_dumbbell(W, h0/4, 3, m, ctr, rad, npts,
                                            stop_tol=tol)
