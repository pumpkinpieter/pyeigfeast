"""DPG EXAMPLE:                  [Requires ngsolve install]

Use the DPG discretization of the Laplace operator (instead of the
standard finite element method) to construct an approximation to the
spectral projector, run the FEAST algorithm, and obtain eigenvalue
approximations.

"""

from pyeigfeast.spectralproj.dpg import NGvecs, SpectralProjDPG
from pyeigfeast.scripts import exact_eig_unitsqr
from netgen.geom2d import unit_square
import ngsolve as ngs


def inputs_for_dpg(mesh, p):
    """
    Generate inputs for setting up DPG Spectral Projector for
    the approximation of the Dirichlet eigenproblem using finite
    element spaces  determined by input degree "p". The input "mesh"
    is assumed to be so that all places where Dirichlet condition is
    to be imposed has marker 'allbdry'.
    """

    # Finite element spaces for the DPG method:

    U = ngs.H1(mesh, order=p, dirichlet="allbdry", complex=True)
    Q = ngs.HDiv(mesh, order=p-1, orderinner=0, complex=True)
    Y = ngs.L2(mesh, order=p+1, complex=True)
    component_spaces = [U, Q, Y]
    X = ngs.FESpace(component_spaces, complex=True)
    uindex = 0
    yindex = 2

    # DPG method uses b( (u,q),d ) = (grad u,grad d) + <q.n,d>

    u, q, e = X.TrialFunction()
    w, r, d = X.TestFunction()
    n = ngs.specialcf.normal(mesh.dim)
    gradu = u.Deriv()
    gradw = w.Deriv()
    grade = e.Deriv()
    gradd = d.Deriv()

    # Form   -b((u,q),d) - conjugate( b((w,r),e) ):

    minus_bb = []
    minus_bb += [ngs.SymbolicBFI(-gradu * gradd - grade * gradw)]
    minus_bb += [ngs.SymbolicBFI(-q*n*d, element_boundary=True)]
    minus_bb += [ngs.SymbolicBFI(-e*r*n, element_boundary=True)]

    # Form  Y inner product:

    y_integrators = [ngs.SymbolicBFI(grade * gradd + e * d)]

    # Make Laplace operator A and mass M on U:

    u = U.TrialFunction()
    w = U.TestFunction()
    gradu = u.Deriv()
    gradw = w.Deriv()
    mss = ngs.BilinearForm(U, symmetric=True)
    mss += ngs.SymbolicBFI(u * w)
    mss.Assemble()
    a = ngs.BilinearForm(U, symmetric=True)
    a += ngs.SymbolicBFI(gradu * gradw)
    a.Assemble()

    return (minus_bb, y_integrators, a.mat, mss.mat,
            X, component_spaces, uindex, yindex, mesh)


def eig_dpg_feast_dir_unitsqr(h, p, m, ctr, rad, npts,
                              verbose=True, quadrule='circ_trapez_shift',
                              check_contour=3, rhoinv=0, inverse=None):
    """
    Apply FEAST with DPG discretization on unit square.

    lam, Y, history, P = eig_dpg_feast_dir_unitsqr(h, p, m, ctr, rad, npts)

    See eig_dpg_feast_dir for documentation of inputs and outputs.
    """
    ngs.ngsglobals.msg_level = 0
    ngm = unit_square.GenerateMesh(maxh=h)
    mesh = ngs.Mesh(ngm)
    for i, bdries in enumerate(mesh.GetBoundaries()):
        ngm.SetBCName(i, 'allbdry')

    return eig_dpg_feast_dir(mesh, p, m, ctr, rad, npts,
                             exactsol=exact_eig_unitsqr,
                             verbose=verbose, quadrule=quadrule,
                             check_contour=check_contour, rhoinv=rhoinv,
                             inverse=inverse)


def eig_dpg_feast_dir(mesh, p, m, ctr, rad, npts,
                      verbose=True, quadrule='circ_trapez_shift',
                      check_contour=3,
                      exactsol=exact_eig_unitsqr, stop_tol=1.e-9, rhoinv=0,
                      inverse=None):
    """ Apply FEAST with DPG discretization.

    lam, Y, history, P = eig_dpg_feast_dir(mesh, p, m, ctr, rad, npts)

    INPUTS:

    mesh = netgen mesh
    p = polynomial degree of the computed eigenfunctions
    m = number of vectors spanning the initial eigenspace iterate
    ctr = center of circular countour
    rad = radius of circular countour
    npts = number of quadrature points

    OPTIONAL INPUTS:

    verbose=True:  False turns off too much outputs
    quadrule='circ_trapez_shift': Select another available quadrature rule
    check_contour=n: check if eigenvalues in contour after n iterations
    stop_tol=eps: stop if eigenvalues do not change by more than eps
    exactsol=exact_eig: if exact eigenvalues and eigenunctions are known,
                        then provide a function that returns them modeled
                        after exact_eig(...).

    OUTPUTS:

    lam = approximate eigenvalues computed by FEAST.
    Y = approximate eigenspan computed by FEAST.
    history = (ewerrors, eferrors, ewchanges, residuals, ews),   where
            ewerrors[i][j] = error in j-th eigenvalue at i-th FEAST step,
            eferrors[i][j] = error in j-th eigenfunction at i-th FEAST step,
                             obtained by projecting the exact eigenfn ef[j]:
                             || ef[j] - (A-projection of ef[j] into Y) ||_A.
            ews[i] = eigenvalues computed at the i-th FEAST step.
    """

    # Set up DPG forms for Dirichlet eigenproblem

    minus_bb_integrators, y_integrators,                      \
        Amat, Mmat, X, component_spaces, uindex, yindex, mesh \
        = inputs_for_dpg(mesh, p)

    # Make spectral projection approximation using DPG:

    U = component_spaces[uindex]
    P = SpectralProjDPG(minus_bb_integrators, y_integrators,
                        Amat, Mmat,
                        X, component_spaces, uindex, yindex, mesh,
                        rad, ctr, npts,
                        quadrule=quadrule, verbose=verbose,
                        rhoinv=rhoinv, reduce_sym=True, inverse=inverse)
    Y = NGvecs(U, m, Mmat, verbose=verbose)
    Y.setrandom()

    # Get exact eigenvalues for reporting errors:

    if exactsol is None:
        ewf = None
    else:
        elm, efs = exactsol(ctr, rad)
        ewf = (elm[0, :], efs)

    # Run FEAST algorithm

    lam, Y, history, _ = P.feast(Y, stop_tol=stop_tol,
                                 cut_tol=1.e-6, check_contour=check_contour,
                                 reportresiduals=True, exactewf=ewf)
    Y.draw()

    return (lam, Y, history, P)


if __name__ == '__main__':

    # PARAMETERS:

    h = 0.1      # mesh size for finite element discretization
    p = 2        # polynomial degree of DPG discretization
    m = 4        # number of vectors in initial eigenspace iterate
    ctr = 19.0   # contour center
    rad = 40     # contour radius
    npts = 8     # number of points in trapezoidal rule

    lam, Y, history, \
        P = eig_dpg_feast_dir_unitsqr(h, p, m, ctr, rad, npts,
                                      quadrule='ellipse_trapez_snug',
                                      rhoinv=0.4)
