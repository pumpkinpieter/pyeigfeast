"""
Compute eigenvalues of a circular L-shaped domain (disc with a quarter
removed) using FEAST & FEM discretization.
"""

from dirichletFEM import eig_fem_feast
from utils import print_apparent_rates
from netgen.geom2d import SplineGeometry
import ngsolve as ng
import numpy as np


def make_circular_Lshape(h):
    """
    Make a circular L-shaped domain composed of 3 unit squares.
    """

    geo = SplineGeometry()
    pntlist = [(0, 0), (0, -1), (-1, -1), (-1, 0),
               (-1, 1), (0, 1), (1, 1), (1, 0)]
    pnts = [geo.AppendPoint(*p) for p in pntlist]
    geo.Append(['line', pnts[0], pnts[1]],
               bc='allbdry', leftdomain=0, rightdomain=1)
    geo.Append(['spline3', pnts[1], pnts[2], pnts[3]],
               bc='allbdry', leftdomain=0, rightdomain=1)
    geo.Append(['spline3', pnts[3], pnts[4], pnts[5]],
               bc='allbdry', leftdomain=0, rightdomain=1)
    geo.Append(['spline3', pnts[5], pnts[6], pnts[7]],
               bc='allbdry', leftdomain=0, rightdomain=1)
    geo.Append(['line', pnts[7], pnts[0]],
               bc='allbdry', leftdomain=0, rightdomain=1)
    m = geo.GenerateMesh(maxh=h)
    return m


def fem_feast_circLshape(h, p, m, ctr, rad, npts, stop_tol=1.e-9,
                         quadrule='circ_trapez_shift',
                         reduce_by_symmetry=True, rhoinv=0, Y=None):

    mesh = ng.Mesh(make_circular_Lshape(h))
    mesh.Curve(max(2, p))

    return eig_fem_feast(mesh, p, m, ctr, rad, npts,
                         stop_tol=stop_tol, quadrule=quadrule, rhoinv=rhoinv,
                         reduce_by_symmetry=reduce_by_symmetry, Y=Y)


def rates_circLshape(maxr, p, m, ctr, rad, npts, h0=1.0, l0=0,
                     quadrule='circ_trapez_shift', rhoinv=0.0,
                     stop_tol=1e-9, niterations=50, verbose=True):
    """
    Perform a sequence of mesh refinements starting from coarse mesh of
    element size h0, run feast starting from refinement l0, and
    estimate apparent convergence rates.
    """

    ng.ngsglobals.msg_level = 0
    mesh = ng.Mesh(make_circular_Lshape(h0))
    mesh.Curve(max(2, p))
    ews = []
    hs = []
    print('Starting from coarse mesh of h = ', h0)
    for ll in range(0, l0):
        print('... refining mesh ...')
        mesh.Refine()
        mesh.Curve(max(2, p))

    for ll in range(l0, l0+maxr):

        h = h0 * 2**(-ll)
        hs.append(h)
        print('Case p=%d, h=%g: Computing ...' % (p, h))

        lam = eig_fem_feast(mesh, p, m, ctr, rad, npts,
                            quadrule=quadrule, verbose=verbose,
                            stop_tol=stop_tol, rhoinv=rhoinv,
                            niterations=niterations)[0]
        ews.append(lam)
        mesh.Refine()
        mesh.Curve(max(2, p))

    ew = np.array(ews)
    print_apparent_rates(ew, hs)
    return (ews, hs)


if __name__ == '__main__':

    m = 6        # number of vectors in initial eigenspace iterate
    a = 10       # Look in the interval [a, b]
    b = 48
    npts = 8     # Number of points in trapezoidal rule
    p = 3        # Polynomial degree of DPG discretization
    h0 = 0.5     # Coarsest mesh size

    ctr = (a+b)/2
    rad = (a-b)/2

    # # To run convergence study:
    # maxr = 3     # max num refinements (only for convergence study)
    # ews, hs = rates_circLshape(maxr, p, m, ctr, rad, npts, h0=h0, l0=0)

    # To perform a single run:
    lam, Y, history, P, A, B, X \
        = fem_feast_circLshape(h0, p, m, ctr, rad, npts)
