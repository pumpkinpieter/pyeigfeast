"""

Compute eigenvalues of L-shaped domain using feast/dpg.

"""

from dirichletDPG import eig_dpg_feast_dir
from utils import print_ew_rates, print_apparent_rates
from netgen.geom2d import SplineGeometry
import ngsolve as ngs
import numpy as np


def make_Lshape(h):
    """
    Make an L-shaped domain composed of 3 unit squares.
    """

    geometry = SplineGeometry()
    pntlist = [(0, 0), (1, 0), (1, -1), (-1, -1), (-1, 1), (0, 1)]
    pnts = [geometry.AppendPoint(*p) for p in pntlist]
    seglist = [(pnts[0], pnts[1], 'allbdry', 0, 1),
               (pnts[1], pnts[2], 'allbdry', 0, 1),
               (pnts[2], pnts[3], 'allbdry', 0, 1),
               (pnts[3], pnts[4], 'allbdry', 0, 1),
               (pnts[4], pnts[5], 'allbdry', 0, 1),
               (pnts[5], pnts[0], 'allbdry', 0, 1)]
    for p0, p1, bn, ml, mr in seglist:
        geometry.Append(['line', p0, p1],
                        bc=bn, leftdomain=ml, rightdomain=mr)
    m = geometry.GenerateMesh(maxh=h)
    return m


def run_feast_Lshape(h, p, m, ctr, rad, npts, stop_tol=1.e-9,
                     quadrule='circ_trapez_shift', rhoinv=0.0):

    ngs.ngsglobals.msg_level = 0
    mesh = ngs.Mesh(make_Lshape(h))
    return eig_dpg_feast_dir(mesh, p, m, ctr, rad, npts,
                             stop_tol=stop_tol, exactsol=None,
                             quadrule=quadrule, rhoinv=rhoinv)


def rates_shapeL(maxr, p, m, ctr, rad, npts, h0=0.25, l0=0,
                 quadrule='circ_trapez_shift', rhoinv=0.0,
                 outputfile='outputs/tmpshapeL.npz'):
    """
    Perform a sequence of mesh refinements starting from coarse mesh of
    element size h0, run feast starting from refinement l0, and
    estimate apparent convergence rates.
    """

    ngs.ngsglobals.msg_level = 0
    ngm0 = make_Lshape(h0)
    mesh = ngs.Mesh(ngm0)

    ews = []
    hs = []
    res = []

    print('Starting from coarse mesh of h = ', h0)
    for ll in range(0, l0):
        print('... refining mesh ...')
        mesh.Refine()

    for ll in range(l0, l0+maxr):

        h = h0 * 2**(-ll)
        hs.append(h)
        print('Case h=%g: Computing ...' % h)

        lam, Y, history, P = eig_dpg_feast_dir(mesh, p, m, ctr, rad, npts,
                                               exactsol=None,
                                               verbose=False,
                                               quadrule=quadrule,
                                               rhoinv=rhoinv)
        ewerrors, eferrors, ewhdist, residuals, ews, converged = history
        ews.append(lam)
        res.append(residuals[-1])
        mesh.Refine()

    ew = np.array(ews)
    print_apparent_rates(ew, hs)

    np.savez(outputfile, ews=ews, hs=hs, res=res)
    return (ews, hs, res)


def print_rates_using_TreffBetcke():
    """
    Load computed eigenvalues from file and tabulate eigenvalue errors
    assuming that the values reported in [Trefethen & Betcke, 2006]
    are the truth.
    """

    # Trefethen + Betcke reports:
    ewTB = [9.6397238, 15.197252, 19.739209, 29.521481, 31.912636, 41.474510]

    # Compare with our computed values:
    npf = np.load('outputs/shapeL/p2r6e3.npz')
    ews = npf['ews']
    hs = npf['hs']
    ewerr = abs(ews - ewTB[:3])
    print(ewerr)
    print_ew_rates(ewerr, hs)


if __name__ == '__main__':

    m = 3        # number of vectors in initial eigenspace iterate
    ctr = 15.0   # contour center
    rad = 6      # contour radius
    npts = 4     # number of points in trapezoidal rule
    p = 2        # polynomial degree of DPG discretization
    maxr = 3     # max num refinements (only for convergence study)
    h0 = 0.25    # coarsest mesh size

    # To run convergence study:
    # rates_shapeL(maxr, p, m, ctr, rad, npts, h0=0.25, l0=1)

    # To perform a single run:
    lam, Y, history, P = run_feast_Lshape(h0/4, p, m, ctr, rad, npts)
