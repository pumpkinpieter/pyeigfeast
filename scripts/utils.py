"""
Simple utility functions used by other scripts
"""

import numpy as np
import ngsolve as ng
from math import log


def exact_eig_unitsqr(ctr, rad, maxeigindex=1000):
    """
         elm, efs = exact_eig(ctr, rad, maxeigindex=1000)

    Return the exact Dirichlet eigenvalues and eigenfunctions of the
    Unit Square, enclosed by a circular contour of radius "rad"
    centered at "ctr". Since the spectrum of the Dirichlet operator is
    an inifinite set, we only search the finite set of first
    "maxeigindex" exact eigenvalues to see which of them falls within
    the contour. The output elm is such that

        λᵢ, l, m = elm[:, i]

    gives the i-th Dirichlet eigenvalue λᵢ = π² (l² + m²).
    """

    # exact ews are of the form  pi^2 * (l^2 + m^2)

    ell = np.arange(1, maxeigindex+1)
    ll = ell ** 2
    LL, MM = np.meshgrid(ll, ll)
    L, M = np.meshgrid(ell, ell)
    exactew = np.pi * np.pi * (LL + MM)
    eL = L.reshape(1, maxeigindex**2)[0]
    eM = M.reshape(1, maxeigindex**2)[0]
    eew = exactew.reshape(1, maxeigindex**2)[0]

    # sort the exact ews and select only those within contour

    e = np.array([eew, eL, eM])
    elm = e[:, e[0, :].argsort()]
    ELM = elm[:, np.where((elm[0, :] < ctr + rad) &
                          (elm[0, :] > ctr - rad))[0]]

    # make corresponding eigenfunctions as ngspy coefficients

    efs = []
    for i in range(ELM.shape[1]):
        e, l, m = ELM[:, i]
        efs.append(ng.sin(np.pi*l*ng.x) * ng.sin(np.pi*m*ng.y))

    return (ELM, efs)


def print_apparent_rates(ew, hs):
    """
    Given a sequence of eigenvalue approximations in "ew" (not
    errors!) examine their successive differences and differences with
    the finest eigenvalue to estimate an apparent convergence rate.
    """

    print("\nAPPARENT CONVERGENCE RATES:")
    ewfinest = ew[-1, :]
    ewerr = np.abs(ew - ewfinest)
    ewerr = ewerr[:-1, :]
    ewdiff = np.abs(ew[1:, :] - ew[:-1, :])

    for j in range(ewerr.shape[1]):
        print("--------------------------CASE OF EW(%1d)-----------------"
              % j + "-----------+")
        print("h,     |EW-FinestEW|:rate, |EW(h)-EW(2h)|:rate,     EW    " +
              "         |")
        print("----------------------------------------------------------"
              + "---------+")

        for i in range(ewerr.shape[0]):
            if i > 0:
                line = 'h/%-5d ' % 2**i
            else:
                line = 'h=%4.3f ' % hs[i]

            # difference between finest ew and current mesh ews
            line += ' %7.4e: ' % abs(ewerr[i][j])
            if abs(ewerr[i][j]) > 1.e-15 and i > 0:
                r = log(abs(ewerr[i-1][j]/ewerr[i][j]))/log(2)
                line += format(r, '+5.2f')
            else:
                line += '  -  '

            # difference between ews after one mesh refinement
            line += '   %7.4e: ' % abs(ewdiff[i][j])
            if abs(ewdiff[i][j]) > 1.e-15 and i > 0:
                r = log(abs(ewdiff[i-1][j]/ewdiff[i][j]))/log(2)
                line += format(r, '+5.2f')
            else:
                line += '  -  '

            # eigenvalues
            line += '   %12.10e ' % ew[i][j]
            print(line + ' |')

        # finest eigenvalue
        line = 'Finest EW:                                       ' + \
               '%12.10e ' % ew[i+1][j]
        print(line + ' |')
    print("----------------------------------------------------------"
          + "---------+")


def print_rates(ewerr, efAerr, hs):
    """
    Tabulate eigenvalue errors (ewerr) and eigenfunction errors (efAerr)
    together with their rates assuming that meshsize (hs) is decreased
    successively by half.
    """

    print("\nCONVERGENCE RATES:")

    for j in range(ewerr.shape[1]):
        print("-------------CASE OF EW(%1d)-----------------+" % j)
        print("  h       EWerror   rate    EFerror   rate |")
        print("-------------------------------------------+")

        for i in range(ewerr.shape[0]):
            if i > 0:
                line = 'h/%-5d ' % 2**i
            else:
                line = 'h=%4.3f ' % hs[i]

            # eigenvalue errors:
            line += ' %7.4e ' % abs(ewerr[i, j])
            if abs(ewerr[i, j]) > 1.e-15 and i > 0:
                try:
                    r = log(abs(ewerr[i-1, j]/ewerr[i, j]))/log(2)
                    line += format(r, '+5.2f')
                except ValueError:
                    line += ' -E- '
            else:
                line += '  -  '

            # eigenfunction errors:
            line += ' %7.4e ' % abs(efAerr[i, j])
            if abs(efAerr[i, j]) > 1.e-15 and i > 0:
                r = log(abs(efAerr[i-1, j]/efAerr[i, j]))/log(2)
                line += format(r, '+5.2f')
            else:
                line += '  -  '
            print(line + ' |')
        print("-------------------------------------------+")


def print_ew_rates(ewerr, hs):
    """
    Tabulate eigenvalue errors (ewerr) with their rates assuming that
    meshsize (hs) is decreased successively by half.
    """

    print("\nCONVERGENCE RATES:")

    for j in range(ewerr.shape[1]):
        print("-----CASE OF EW(%1d)---------+" % j)
        print("  h       EWerror   rate   |")
        print("---------------------------+")

        for i in range(ewerr.shape[0]):
            if i > 0:
                line = 'h/%-5d ' % 2**i
            else:
                line = 'h=%4.3f ' % hs[i]

            # eigenvalue errors:
            line += ' %7.4e  ' % abs(ewerr[i, j])

            if abs(ewerr[i, j]) > 1.e-15 and i > 0:
                r = log(abs(ewerr[i-1, j]/ewerr[i, j]))/log(2)
                line += format(r, '+5.2f')
            else:
                line += '  -  '
            print(line + ' |')
        print("---------------------------+")


def print_ew_err_rates(exew, ew, hs):
    """
    Given exact eigenvalues (exew) and computed eigenvalues (ew),
    tabulate eigenvalue errors with their rates assuming that
    meshsize (hs) is decreased successively by half.
    """

    print("\nCONVERGENCE RATES:")

    ex = np.array(exew)
    ew = np.array(ew)
    ewerr = np.abs(ew - ex)

    for j in range(ewerr.shape[1]):
        print("--------------------CASE OF EW(%1d)----------------+" % j)
        print("  h           Computed EW       EWerror   rate   |")
        print("-------------------------------------------------+")

        for i in range(ewerr.shape[0]):
            if i > 0:
                line = 'h/%-5d ' % 2**i
            else:
                line = 'h=%4.3f ' % hs[i]

            # eigenvalue
            line += ' %20.16g ' % ew[i, j]

            # eigenvalue errors:
            line += ' %7.4e  ' % abs(ewerr[i, j])

            if abs(ewerr[i, j]) > 1.e-15 and i > 0:
                r = log(abs(ewerr[i-1, j]/ewerr[i, j]))/log(2)
                line += format(r, '+5.2f')
            else:
                line += '  -  '
            print(line + ' |')
        print("-------------------------------------------------+")
