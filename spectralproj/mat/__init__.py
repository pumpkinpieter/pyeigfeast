"""
Build spectral projectors using numpy matrix approximations of resolvents.
"""
__all__ = ['Vecs', 'SpectralProjMat']

from .spectralprojmat import Vecs, SpectralProjMat
