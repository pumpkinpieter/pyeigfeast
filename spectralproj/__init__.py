"""
Base class for approximating spectral projectors
"""
__all__ = ['Span', 'SpectralProj', 'simple_multiple_zoom']

from .spectralproj import Span, SpectralProj
from .splitzoom import simple_multiple_zoom
