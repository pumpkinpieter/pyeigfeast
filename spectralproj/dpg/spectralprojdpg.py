from pyeigfeast.spectralproj import SpectralProj, simple_multiple_zoom
from ..ngs import NGvecs
import numpy as np
import ngsolve as ngs
from ngsolve import TaskManager
from scipy.sparse import coo_matrix
from scipy.linalg import norm


class SpectralProjDPG(SpectralProj):

    """This class approximates the spectral projector of an operator
    AA : UU -> UU, namely

    PP = (2 pi i)^-1   integral_over_contour  (z-AA)^-1  dz,

    by an approximation P which is obtained by approximating AA by the
    primal DPG method and the integral by the trapezoidal rule. The
    background for this class and the notations assigned to class data
    members follow this description:

    * UU is a Hilbert space (infinite dimensional). We are interested
      in approximating an eigenpair l, y satisfying AA * y = l * y.

    * U = a finite element subspace of UU, wherein the
      eigenvectors are computed.

    * The DPG approximation of the resolvent (z - AA)^-1 * f, for any
      f in U, is the function u in U computed by solving e in Y, u in
      U, and q in Q, satisfying

      y(e,d) + z * s(u,d) - b((u,q),d) = s(f,d)   for all d in Y,
               z * s(w,e) - b((w,r),e) = 0        for all w in U, r in Q.

      The left hand side is written in combined Hermitian form as

      B(u,q,e|w,r,d) =  y(e,d) + z * s(u,d) - b((u,q),d)
                     + conjugate(z * s(w,e) - b((w,r),e)).

    * Above, Y and Q are auxiliary spaces used by the DPG method to
      approximate error representation and other/interface variables.
      We assume that U is contained in Y.

    * y(.,.) = inner product of Y.

    * b(.,.) : (U x Q) x Y -> C, primal DPG weak form for AA.

    * s(.,.) : U x Y -> C,  form used to transfer functions from U to Y.

    * X = X[0] x X[1] x ...  is a Cartesian product of finite element
      spaces used by the DPG space to approximately solve (z - AA)u=f.
      Both U and Y are present as component spaces of X.

    * m(.,.) is the inner product of U and the Gram matrix of this
      inner product with respect to the working basis of U is M.

    * A is the stiffness matrix of the form   m(AA u,w).
    """

    def __init__(self, minus_bb_integrators, y_integrators,
                 A, M, X, spaces, uindex, yindex, mesh,
                 radius, center, npts, reduce_sym=False,
                 quadrule='circ_trapez_shift', rhoinv=0.0,
                 verbose=True, inverse=None):
        """Constructor of DPG spectral projection approximation:

        INPUTS:

        * y_integrators = list of ngsolve bilinear form integrators
          (bfi) to implement y(e,d).

        * minus_bb_integrators = list of ngsolve bfi's on X that
          implement  -b((u,q),d) - conjugate( b((w,r),e) ).

        * ss_integrators = list of ngsolve bfi to implement
          alpha * s(u,d) + beta * s(w,e).

        * M = mass matrix of m(u,w) inner product using U's basis.

        * A = stiffness matrix of the form m(A u, w) using U's basis.

        * mesh = finite element mesh as an ngsolve Mesh object.

        * X = finite element space on mesh containing U, Y and other
          needed component spaces.

        * uindex, yindex are (0-based) indices into X such that
          U = X[uindex] and Y = X[yindex].

        * radius, center, npts: Spectral projector approximation is
          based on an integral around a circular contour of given
          'center' and 'radius', discretized using the trapezoidal
          rule with 'npts' number of points. If 'npts' is odd, it is
          converted to the next higher even number.

        * reduce_sym: Reduce the number of quadrature points assuming
          that the resolvent approximation R(z) at a point z
          satisfies   R(conj(z)) * f = conj( R(z) * conj(f) ).

        * inverse: Specifies the type of inverse to use when computing
          the approximations to the resolvents at the quadrature nodes.

        """

        self.X = X
        self.component_spaces = spaces
        self.U = spaces[uindex]
        self.uind = uindex
        self.yind = yindex
        self.mesh = mesh
        self.minus_bb_integ = minus_bb_integrators
        self.y_integ = y_integrators
        self.M = M
        self.A = A
        self.npts = npts

        super().__init__(radius, center, npts,
                         quadrule=quadrule, verbose=verbose, rhoinv=rhoinv,
                         reduce_sym=reduce_sym, inverse=inverse)

        self.Xscratch1 = ngs.GridFunction(self.X)
        self.Xscratch2 = ngs.GridFunction(self.X)
        self.Uscratch = ngs.GridFunction(self.U)
        self.Uscratch2 = ngs.GridFunction(self.U)

        if self.reduce_sym:
            self._reduce_quad_points()
        else:
            self.z_reduced = self.z
            self.reflected = [False]*len(self.z)
            self.w_reflect = []

        self._make_resolvents()

        s = ngs.BilinearForm(self.X)
        for integrator in self._s_integ(1.0):
            s += integrator
        s.Assemble()
        self.S = s.mat

    def reduce_by_symmetry(self):
        """
        Reduce the number of quadrature points assuming that
           R(conj(z)) * f = conj( R(z) * conj(f) ).
        When this assumption holds, the resolvent approximations
        are not made at conjugate quadrature points.
        """

        self._prnt('SpectralProjDPG: Attempting reduction by symmetry ' +
                   'and recomputing factorizations')

        self.reduce_sym = True
        self._reduce_quad_points()
        self._make_resolvents()

    def _s_integ(self, alpha, beta=0):
        """ Make the form  alpha * s(u,d) + beta * s(w,e). """

        trial = self.X.TrialFunction()
        test = self.X.TestFunction()
        u = trial[self.uind]
        d = test[self.yind]
        e = trial[self.yind]
        w = test[self.uind]
        if beta == 0:
            return [ngs.SymbolicBFI(alpha * u * d)]
        else:
            return [ngs.SymbolicBFI(alpha * u * d + beta * e * w)]

    def _make_resolvents(self):
        """Compute and store the approximations R[i] to the resolvent

        inv( z[i] - AA )

        at each quadrature point z[i]. This is done by assembling the
        DPG bilinear form for z - AA, namely

        B(u,q,e|w,r,d) =  y(e,d) + z * s(u,d) - b((u,q),d)
                       + conjugate(z * s(w,e) - b((w,r),e)),

        and then inverting the assembled matrix.  Note that only the Schur
        complement (wrt inner dofs) inverse is stored for efficiency.
        Resolvents are computed only for half the points due to symmetry.
        """

        self._prnt('SpectralProjDPG: ' +
                   'Computing DPG resolvents along the contour:')

        self.z_minus_A = []   # list of assembled z - A
        self.R = []           # R[i] = inv( z[i] * B - A )

        u, q, e = self.X.TrialFunction()
        w, r, d = self.X.TestFunction()

        def setup_resolvents():
            """
            Make these data members:

            self.z_minus_A[i] = the DPG discretization of the shifted
                operator z - A (assembled) with z = self.z_reduced[i].

            self.R[i] = the resolvent R(z) when z = self.z_reduced[i].

            """

            for zz in self.z_reduced:
                self._prnt('SpectralProjDPG:   ' +
                           'Factorizing at z = %+7.3f% + 7.3fj'
                           % (zz.real, zz.imag))

                # z_a = ngs.BilinearForm(self.X, symmetric=False,
                #                        flags={"eliminate_internal": True})

                z_a = ngs.BilinearForm(self.X, symmetric=False,
                                       eliminate_internal=True)
                for integrator in self.y_integ:
                    z_a += integrator
                for integrator in self.minus_bb_integ:
                    z_a += integrator
                for integrator in self._s_integ(zz, zz.conjugate()):
                    z_a += integrator

                try:
                    z_a.Assemble()
                except RuntimeError as exc:
                    self._prnt('   Exception:', exc.__str__())
                    self._prnt('   Ran out of heap memory!')
                    self._prnt('   Doubling heap and retrying ...')
                    ngs.SetHeapSize(2*1000000)
                    z_a.Assemble()

                self.z_minus_A.append(z_a)

                if self.inverse is None:
                    self.R.append(z_a.mat.Inverse(self.X.FreeDofs(True)))
                else:
                    self.R.append(z_a.mat.Inverse(self.X.FreeDofs(True),
                                                  inverse=self.inverse))

        with TaskManager():
            setup_resolvents()

    def apply_resolvent(self, i, v):
        """
        Return DPG approximation to  u  ~  (z[i] - AA)^-1 * v.
        Input v and output u are both in the space U.

        """

        # 1. Compute Sv = S * v, the data transfer to DPG system
        vv = self.Xscratch1
        vv.vec[:] = 0
        vv.components[self.uind].vec.data = v

        sv2 = self.Xscratch2
        sv2.vec[:] = 0
        sv2.vec.data = self.S * vv.vec

        vv.vec[:] = 0
        vv.components[self.yind].vec.data = sv2.components[self.yind].vec
        Sv = vv.vec

        # 2. Apply resolvent R[i] to S * v
        u = self.Xscratch2
        u.vec[:] = 0

        # 2(a): Since interior unknowns have been eliminated, we
        # modify rhs Sv to give to the Schur complement system
        Sv.data += self.z_minus_A[i].harmonic_extension_trans * Sv

        # 2(b): Apply Schur complement inverse
        u.vec.data = self.R[i] * Sv

        # 2(c): Add correction to condensed dofs due to inner dofs
        u.vec.data += self.z_minus_A[i].harmonic_extension * u.vec

        # 2(d): Solve for inner dofs
        u.vec.data += self.z_minus_A[i].inner_solve * Sv

        # 3. Extract U-component of the solution and return
        self.Uscratch.vec.data = u.components[self.uind].vec

        return self.Uscratch.vec

    def projectA(self, e, Y):
        """
        Project the vector e onto the span object Y  in the A-norm.
        (Note that e should be input as BaseVector of U gridfunction.)

        """

        # We need to make and solve the small dense system
        # (A * p, y[i]) = (A * e, y[i])
        # where Span[y[0], ..., y[m]] = Y.

        yAy, _ = self.rayleigh(Y)
        Ae = self.Uscratch.vec
        Ae.data = self.A * e

        Aey = np.zeros((Y.m, 1), dtype=complex)
        for j in range(Y.m):

            Aey[j] = ngs.InnerProduct(Ae, Y._mv[j])

        try:
            p = np.linalg.solve(yAy, Aey)  # Solve small proj system for p

        except np.linalg.linalg.LinAlgError as err:

            self._prnt('   Inversion of yAy failed!')
            self._prnt('   Numpy reports: ' + err.__str__())
            self._prnt('   Reorthogonalizing and trying again...')

            Y.orthoreduce(1.e-9)

            return self.projectA(e, Y)

        Pe = Ae.CreateVector()
        Pe[:] = 0                      # Recover large proj vector Pe

        for j in range(Y.m):
            Pe.data += complex(p[j][0]) * Y._mv[j]

        return Pe

    def zoom(self, ew, delta=None):
        """simple, multiple = SpectralProjDPG.zoom(ew, delta)

        Computes spectral projectors, one corresponding to each
        eigenvalue in "ew", counting those eigenvalues that differ by
        less than "delta" as approximations of an eigenvalue of
        multiplicity>1.  The outputs "simple" and "multiple" are dicts
        documented in spectralproj.simple_multiple_zoom. This method
        adds 'specproj' to these dicts, the corresponding spectral projector
        object.
        """

        s, m = simple_multiple_zoom(self.c-self.r, self.c+self.r, ew, delta)
        specproj = []
        for j in range(len(s['index'])):
            specproj += [SpectralProjDPG(self.minus_bb_integ, self.y_integ,
                                         self.A, self.M, self.X,
                                         self.component_spaces,
                                         self.uind, self.yind, self.mesh,
                                         s['radius'][j], s['center'][j],
                                         self.npts, self.reduce_sym,
                                         self.quadrule, self.rhoinv,
                                         self.verbose)]
        s['specproj'] = specproj

        specproj = []
        for j in range(len(m['index'])):
            specproj += [SpectralProjDPG(self.minus_bb_integ, self.y_integ,
                                         self.A, self.M, self.X,
                                         self.component_spaces,
                                         self.uind, self.yind, self.mesh,
                                         m['radius'][j], m['center'][j],
                                         self.npts, self.reduce_sym,
                                         self.quadrule, self.rhoinv,
                                         self.verbose)]
        m['specproj'] = specproj

        return s, m

    def compute_errror_discrete2exact(self, Y, lam, exactewf):
        """
        Compute and report eigenvalue and eigenvector errors,
        but compared against the span of the interpolants of
        the exact eigenfunctions contained in exactewf. The eigenvector
        error measure reported is

         distance FROM computed eigenfn TO exact (interpolated) eigenspace.

        Note that another method compute_error(...) yields the alternate
        eigenspace error measure

         distance FROM interpolated exact eigenfunction TO computed eigenspace.

        INPUT: exactewf = (ew, ef) where
                  ew[l] = l-th exact eigenvalue, sorted by real part,
                  ef[l] = corresponding eigenfunction as ngspy coefficient.
               Y = computed eigenspace span (with basis y[i] representing
                   output eigenfunctions)
               lam = computed eigenvalues.
        OUTPUT:
               ewdiff[l] = ewh[l] - ew[l],
                           where ewh equals lam sorted by its real part.
               efdiff_discrete2exact[l] =
                        || y[l] - (projection of y[l] into interpE) ||_A.
                           where eih is the lth eigenfunction computed by
                           FEAST, and interpE is set of exact eigenfunctions
                           interpolated.
        """

        # The exact eigenvalues and eigenfunctions.
        ew, ef = exactewf

        # A scratch grid function and vector we will be using for later
        # computations.
        vi = self.Uscratch2
        tmp = self.Uscratch.vec

        # Begin by creating a span object to hold the interpolants of the
        # exact eigenfunctions specified in exactewf.
        interpE = NGvecs(self.U, len(ef), self.M, self.verbose)
        interpE.zerodata()

        # Now run through the exact eigenvectors and set the spans
        for i in range(len(ef)):
            # Compute the interpolant of the exact eigenfunction ef[i]
            vi.vec[:] = 0
            vi.Set(ef[i])

            # Copy the data over from the interpolated eigenfunction.
            interpE._mv[i].data = vi.vec

        # The distances of the given approximate eigenfunctions computed by
        # FEAST from the span of the interpolated exact eigenfunctions.
        efdiff_discrete2exact = np.ones(Y.m)*1e100

        # Now run through the vectors in the span of Y to compute the
        # distances of the eigenfunctions from the span of the interpolated
        # exact eigenfunctions.
        for i in range(Y.m):
            # Extract the ith eigenvector computed by FEAST from the span Y.
            eih = Y._mv[i]

            # Compute the projection of the approximated eigenfunction onto
            # the span of interpolated exact eigenvectors.
            Peih = self.projectA(eih, interpE)

            # Compute the complementary projection applied to eih.
            Peih.data -= eih

            # Compute the error in the a(.,.) inner product.
            tmp.data = self.A * Peih
            efdiff_discrete2exact[i] = np.sqrt(
                abs(ngs.InnerProduct(tmp, Peih)))

        self._prnt('   Inteprolated Eigenspace diff:\n   ' +
                   np.array_str(efdiff_discrete2exact))

        ewh = np.sort(lam.real)              # computed eigenvalues

        self._prnt('   Current FEAST approximations:\n   ' +
                   np.array_str(ewh))
        im = norm(lam.imag, 2)
        if im > 1.e-10:
            self._prnt('***Computed eigenvalues are not real!')
        if len(ewh) == len(ew):
            self._prnt('   |Current Eigenvalue Errors| = \n   ' +
                       np.array_str(abs(ewh-ew)))
        else:
            self._prnt('   Computed and exact eigenspace dimensions differ!')

        mm = min(len(ewh), len(ew))
        ewdiff = ewh[:mm] - ew[:mm]

        return (ewdiff, efdiff_discrete2exact)

    def compute_error(self, Y, lam, exactewf):
        """
        Compute and report eigenvalue and eigenvector errors. Eigenvector
        error measure reported is the distance FROM interpolated
        exact eigenfunction TO computed eigenspace.

        INPUT: exactewf = (ew, ef) where
                  ew[l] = l-th exact eigenvalue, sorted by real part,
                  ef[l] = corresponding eigenfunction as ngspy coefficient.
               Y = computed eigenspace span.
               lam = computed eigenvalues.
        OUTPUT:
               ewdiff[l] = ewh[l] - ew[l],
                           where ewh equals lam sorted by its real part.
               efdiff[l] = || ef[l] - (projection of ef[l] into Y) ||_A.
        """
        ew, ef = exactewf
        yAy, _ = self.rayleigh(Y)
        vi = self.Uscratch2
        tmp = self.Uscratch.vec
        efdiff = np.ones(len(ef))*1e100

        for i in range(len(ef)):

            vi.vec[:] = 0
            vi.Set(ef[i])                     # vi = interpolant of ef[i]
            Pvi = self.projectA(vi.vec, Y)    # Pvi = A-projection(vi) on Y
            Pvi.data -= vi.vec                # Pvi = Pvi - vi
            tmp.data = self.A * Pvi           # tmp = A * (Pvi - vi)
            efdiff[i] = np.sqrt(abs(ngs.InnerProduct(tmp, Pvi)))

        self._prnt('   Eigenspace diff:\n   ' + np.array_str(efdiff))

        ewh = np.sort(lam.real)              # computed eigenvalues
        # self._prnt('   Exact undiscretized Dirichlet eigenvalues:\n   ' +
        #            np.array_str(ew))
        self._prnt('   Current FEAST approximations:\n   ' +
                   np.array_str(ewh))
        im = norm(lam.imag, 2)
        if im > 1.e-10:
            self._prnt('***Computed eigenvalues are not real!')
        if len(ewh) == len(ew):
            self._prnt('   |Current Eigenvalue Errors| = \n   ' +
                       np.array_str(abs(ewh-ew)))
        else:
            self._prnt('   Computed and exact eigenspace dimensions differ!')

        mm = min(len(ewh), len(ew))
        ewdiff = ewh[:mm] - ew[:mm]
        return (ewdiff, efdiff)

    def matrices_scipy(self):
        """
        Return sparse matrices related to DPG resolvent computation.

        To describe the matrices, recall that the resolvent
        approximation u = R_h(z,f) solves

            B(u,q,e|w,r,d) =  s(f,d)

        where

        B(u,q,e|w,r,d) =  y(e,d) + z * s(u,d) - b((u,q),d)
                       +     conj( z * s(w,e) - b((w,r),e) )

        for some forms s and y (usually the L^2 and Y inner products).

        This function returns the following:

           B: stiffness matrix of b(.,.) (with no z-term)

           S: stiffness matrix of s(.,.)

           Bfree: list of free dof indices in B. These are (0-based)
                indices into the degrees of freedom of the space X.
                Indices not in Bfree are nodes that are constrained by
                Dirichlet or other conditions.

           space_dofs: The space X=X0 x X1 x …  has its degrees of
                freedom (dofs) split in component spaces as follows:
                The dofs of Xi are enumerated from space_dofs[i]
                through space_dofs[i+1]-1.

           M: The Gram matrix of the U-inner product (usually L^2
                inner product), which we denote by m(.,.).

           A: The matrix of the form a(u,w)=m(AA u,v) where AA is the
                exact operator.

           Afree: This is similar to Bfree, but the count is using the
                U-basis (instead of the X -basis).

        """

        u, q, e = self.X.TrialFunction()
        w, r, d = self.X.TestFunction()

        # Assemble
        b = ngs.BilinearForm(self.X, symmetric=False)
        for integrator in self.y_integ:
            b += integrator
        for integrator in self.minus_bb_integ:
            b += integrator
        b.Assemble()

        # whole matrix (without bc)
        i, j, v = b.mat.COO()
        B = coo_matrix((v, (i, j)))
        free = np.array(self.X.FreeDofs())
        Bfree = np.where(free)[0]    # free dofs

        ndofs = 0
        space_dofs = [0]

        for s in self.component_spaces:
            ndofs += s.ndof
            space_dofs.append(ndofs)
        if space_dofs[-1] != self.X.ndof:
            print("matrices_scipy: Not all spaces given in input!")
        space_dofs = np.array(space_dofs)

        i, j, v = self.S.COO()
        S = coo_matrix((v, (i, j)))

        i, j, v = self.A.COO()
        A = coo_matrix((v, (i, j)))
        free = np.array(self.U.FreeDofs())
        Afree = np.where(free)[0]    # free dofs

        i, j, v = self.M.COO()
        M = coo_matrix((v, (i, j)))

        return (B, S, Bfree, space_dofs, A, M, Afree)

    def matrix_filter(self):
        """
        Return the matrix approximation S_N^h of the exact spectral
        projector S, as a dense numpy matrix. (Memory intensive!)
        Only the restriction of the matrix to free dofs is returned.
        """

        ndof = self.U.ndof
        y = np.identity(ndof, dtype=complex)
        Y = NGvecs(self.U, ndof, self.M)
        Y.fromnumpy(y)
        PY = self * Y
        S = PY.tonumpy()

        free = np.array(self.U.FreeDofs())
        freedofs = np.where(free)[0]

        SS = S[freedofs, :]
        S = SS[:, freedofs]

        return S

    # Virtual functions needed for FEAST: ############################

    def __mul__(self, v):
        """
        Return P * v where P is the spectral projector object
        and v is a span object.
        """

        with TaskManager():

            Pv = v.zeroclone()
            tmp = v._mv[0].CreateVector()

            for i in range(len(self.z_reduced)):
                for k in range(v.m):

                    # Compute   u = R(z[i]) *  v[k]
                    u = self.apply_resolvent(i, v._mv[k])
                    Pv._mv[k].data += complex(self.w[i]) * u

                    if self.reflected[i]:

                        # Compute   u = conj( R(zr) *  conj(v[k]) )
                        # where zr is the reflection of z[i] about the
                        # real axis.

                        for j in range(len(tmp.data)):
                            tmp[j] = np.conjugate(v._mv[k][j])

                        u = self.apply_resolvent(i, tmp)

                        for j in range(len(tmp.data)):
                            u[j] = np.conjugate(u[j])

                        Pv._mv[k].data += complex(self.w_reflect[i]) * u

        return Pv

    def rayleigh(self, q):
        """ Return matrices qAq and qMq whose entries are

        qAq[i,j] = ( A * q[j], q[i] )
        qMq[i,j] = ( q[j], q[i] )

        """

        qAq = np.zeros((q.m, q.m), dtype=complex)
        qBq = np.zeros((q.m, q.m), dtype=complex)
        tmp = self.Uscratch.vec

        for k in range(q.m):
            for ll in range(k, q.m):
                tmp.data = self.A * q._mv[ll]
                qAq[k, ll] = ngs.InnerProduct(tmp, q._mv[k])
                qAq[ll, k] = np.conjugate(qAq[k, ll])

                tmp.data = self.M * q._mv[ll]
                qBq[k, ll] = ngs.InnerProduct(tmp, q._mv[k])
                qBq[ll, k] = np.conjugate(qBq[k, ll])

        return (qAq, qBq)

    def residual(self, lam, y):

        residnorms = np.zeros(y.m)
        residual = self.Uscratch.vec
        residAinv = residual.CreateVector()

        for ll in range(y.m):
            residual.data = self.M * y._mv[ll]
            nrm = ngs.InnerProduct(y._mv[ll], residual)

            residual.data = complex(-lam[ll]) * residual
            residual.data += self.A * y._mv[ll]
            residAinv.data = self.A.Inverse(self.U.FreeDofs()) * residual

            # Compute || A * y[l] - lam[i] * M * y[l] ||_A^-1
            residnorms[ll] = np.sqrt(abs(ngs.InnerProduct(residual,
                                                          residAinv)/nrm))

            # NOTE: If computing residual in L2-norm (not in Ainv-norm
            # as above), be sure to ZERO OUT non-FreeDofs!

        return residnorms
