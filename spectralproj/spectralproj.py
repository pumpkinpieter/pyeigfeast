"""BASE CLASSES: Span, SpectralProj"""

from numpy import linspace, pi, exp, ones
from scipy.linalg import eigh, eig, LinAlgError
import numpy as np
import matplotlib.pyplot as plt


class Span:
    """ A base class of SPANS of vectors.

    Derived classes must implement orthogonalization, memory creation,
    mapping by operators, etc.

    """

    def __init__(self, n, m, verbose=True):
        self.n = n
        self.m = m
        self.verbose = verbose
        self.err = 'Pure virtual function called in  base Span!'

    def _prnt(self, str):
        if self.verbose is True:
            print(str)

    # The following functions must be implemented by derived classes:######

    def orthoreduce(self, tol):
        """Replace current vectors with an orthogonal basis for the same
        span. Any vectors that do not add substantially to the span,
        as specified by 'tol', will be removed.
        """
        raise NotImplementedError(self.err)

    def remove(self, cut):  # Remove vectors with cut indices
        """From this span object, remove all vectors in those positions listed
        in given array 'cut'.
        """
        raise NotImplementedError(self.err)

    def augment(self):
        """Enrich the given span 'y' by more vectors, or somehow
        with more eigenspace content. This function is called when
        the feast algorithm (below) restarts after a convergence failure.
        """
        raise NotImplementedError(self.err)

    def zeroclone(self):
        """Create memory to return an object of the same size and shape as
        this span, initialize memory to zero, and return it.
        """
        raise NotImplementedError(self.err)

    def scale(self, i, a):
        """Scale i-th vector of this span by a"""
        raise NotImplementedError(self.err)

    def __imul__(self, other):
        """Replace self by self * other. A derived class should implement this
        at least when other is a numpy array (of the right size).
        """
        print('Span * ', type(other), ' not available!')
        raise NotImplementedError(self.err)

    def compute_error(self, Y, lam, exactewf):
        """If exact eigenvalues and eigenfunctions are known and provided in
        argument "exactewf", then compute errors.
        """
        raise NotImplementedError(self.err)

    def compute_error_nsa(self, leftY, rightY, lam, exactewf):
        """If exact eigenvalues and left and right eigenvectors are known
        and provided in argument "exactewf", then compute errors.
        """
        raise NotImplementedError(self.err)


class SpectralProj():
    """A class of SPECTRAL PROJECTIONS

    P = (2 π i)⁻¹ ∮ (z-A)⁻¹  dz

    where the contour integral is approximated by a quadrature.
    (This includes generalized eigenproblems via a base inner product
    of the space A is acting on.)

    Derived classes are created using particular operators A: H -> H,
    where H is a Hilbert space (whose basis, inner product, and any
    other needed information, are maintained by the derived class).
    """

    def __init__(self,
                 radius=None,
                 center=None,
                 npts=None,
                 quadrule='circ_trapez_shift',
                 rhoinv=0.0,
                 within=None,
                 contourquad=None,
                 verbose=True,
                 reduce_sym=False,
                 inverse=None):
        """Construct base spectral projector P.

        INPUT:

        * inverse: The type of sparse factorization (based on
        installed packages) to be used for making resolvents.

        * verbose: outputs and reports.

        * reduce_sym: If True, then reduce number of quadrature points
        assuming that the resolvent approximation R(z) at a point z
        along the contour satisfies R(conj(z)) * f = conj(R(z) * conj(f)).
        (Do not use if conj(z) is not in the contour whenever z is. Do not
        use if the operator is not selfadjoint.)

        * contourquad: If this argument is given, all the previous arguments
        are IGNORED and the quadrature rule is set by
        keys of the dictionary
          contourquad['z']: numpy (1d) array of quadrature points
          contourquad['w']: numpy (1d) array of (complex) weights
          contourquad['within']: a python function that returns an array of
                        true or false depending on whether the input complex
                        number(s) is(are) within the contour or not.
          contourquad['quadrule']: a string/name for this quadrature & contour.

        IF contourquad is None, THEN the contour is assumed to be a
        CIRCLE or ELLIPSE and the following parameters are used:

        * within: a function to override default for checking if a
        point is within the circular contour or not.

        * quadrule: a name (string) for the quadrature rule used to
        approximate the contour integral.

        * radius, center, rhoinv:   With r=radius, c=center,
        if rhoinv = 0, then the contour is the circle given by
           z = c + r exp(iθ),      0 ≤ θ ≤ 2π.
        If rhoinv > 0, then the contour is the Bernstein ellipse
           z = c + [ρ exp(iθ) + ρ⁻¹ exp(-iθ)] r / (ρ + ρ⁻¹).
        When c is real, this gives an ellipse aligned with x/y axes,
        intersecting with the x-axis at c ± r and intersecting with
        the y-axis at ± i r (ρ - ρ⁻¹)/(ρ + ρ⁻¹).

        """

        self.verbose = verbose
        self.reduce_sym = reduce_sym
        self.inverse = inverse
        self.err = 'Function expected in derived class called in SpectralProj!'

        if contourquad is not None:

            # we are given a custom contour and/or quadrature

            self.z = contourquad['z']
            self.w = contourquad['w']
            self.within = contourquad['within']
            self.quadrule = contourquad['quadrule']
            self.n = len(self.z)
            self.ewchar = abs(self.z.mean())

        else:
            # else proceed assuming a circular or elliptical contour

            self.initcircellip(radius, center, npts, quadrule, rhoinv)
            if within is not None:
                self.within = within

    def initcircellip(self, radius, center, npts, quadrule, rhoinv):
        """ circular & elliptical contour intializations """

        if radius is None or center is None or npts is None:
            raise ValueError('Provide radius, center, npts.')
        self.r = radius  # spectrum contained in |z-c| < r
        self.c = center  # c = center of circular contour
        if npts == 1:
            self.n = npts
        elif npts % 2 == 0:
            self.n = npts  # number of points in quadrature rule
        else:
            self.n = npts + 1
        self.rhoinv = rhoinv
        if self.rhoinv > 0:
            if self.rhoinv >= 1:
                raise ValueError('Input rhoinv must be between 0 and 1!')
            self.rho = 1.0 / rhoinv  # rho of a Bernstein ellipse

        # use _compute_quadrature to set contour quadrature pts
        self.quadrule = quadrule.lower()
        self.quad_err = 'Unknown quadrature rule \"' + str(quadrule) + '\".'
        self.z, self.w, self.n = self._compute_quadrature(self.n)

        if self.rhoinv > 0:

            def within_ellipse(z):
                """ Given a numpy array z, return an array of true or false
                depending on whether each z-value is in the ellipse or not."""

                z0 = z - self.c
                xshift = z0.real
                yshift = z0.imag
                e = (self.rho - self.rhoinv) / (self.rho + self.rhoinv)
                return (xshift / self.r)**2 + (yshift /
                                               (self.r * e))**2 - 1 < 0

            self.within = within_ellipse

        else:  # rhoinv=0 => this is a circle

            self.within = lambda z: abs(self.c - z)**2 < self.r**2

        if not isinstance(self.c, complex):
            # a char value to compute relative ew errors
            if (self.c - self.r) * (self.c + self.r) < 0:
                # endpoints of diff sign
                self.ewchar = self.r
            else:
                self.ewchar = abs(self.c)
        else:
            self.ewchar = np.abs(self.c) + self.r

    def _prnt(self, str):
        if self.verbose is True:
            print(str)

    def _compute_quadrature(self, n):

        if n == 1:

            self._prnt('\nSpectralProj: Just doing power iteration')
            z = [self.c]
            w = [1.0]

        elif self.quadrule == 'circ_trapez_shift':

            self._prnt('\nSpectralProj: Setting shifted trapezoidal ' +
                       'rule quadrature on circular contour')
            self._prnt('SpectralProj: Radius=%g, Center=%g%+gj' %
                       (self.r, self.c.real, self.c.imag))
            theta = linspace(0, 2 * pi, num=n, endpoint=False)

            # shift points so that no z is on R if c is on R.
            dtheta = (2 * pi) / n
            theta += 0.5 * dtheta
            z = self.r * exp(1j * theta) + self.c
            w = self.r * exp(1j * theta) / n

        elif self.quadrule == 'circ_trapez_snug':

            self._prnt('\nSpectralProj: Setting snug trapezoidal ' +
                       'rule quadrature on circular contour')
            theta = linspace(0, 2 * pi, num=n, endpoint=False)
            self.eta = self.r / pow(2, 1.0 / self.n)
            z = self.eta * exp(1j * theta) + self.c
            w = self.eta * exp(1j * theta) / n

        elif self.quadrule == 'circ_trapez':

            self._prnt('\nSpectralProj: Setting trapezoidal rule quadrature' +
                       '  on circular contour (without any shift)')
            theta = linspace(0, 2 * pi, num=n, endpoint=False)
            z = self.r * exp(1j * theta) + self.c
            w = self.r * exp(1j * theta) / n

        elif self.quadrule == 'circ_simpsons':

            self._prnt('\nSpectralProj: Setting shifted Simpsons rule ' +
                       '  on circular contour')
            weights = np.array([2 if k % 2 == 0 else 4
                                for k in range(n)]) / (3.0 * n)
            theta = linspace(0, 2 * pi, num=n, endpoint=False)

            # shift points so that no z is on R if c is on R.
            dtheta = (2 * pi) / n
            theta += 0.5 * dtheta
            z = self.r * exp(1j * theta) + self.c
            w = (z - self.c) * weights

        elif self.quadrule == 'ellipse_trapez_shift':

            if self.rhoinv <= 0:
                self._prnt('**** rhoinv = %g' % self.rhoinv)
                raise ValueError('Ellipse contour need rhoinv > 0 ' +
                                 '(or rho < infinity)')

            self._prnt('\nSpectralProj: Setting shifted trapezoidal ' +
                       'rule on elliptical contour')
            theta = linspace(0, 2 * pi, num=n, endpoint=False)
            dtheta = (2 * pi) / n
            theta += 0.5 * dtheta
            self.eta = 2 * self.r / (self.rho + self.rhoinv)
            z = self.c + self.eta * 0.5 \
                * (self.rho * exp(1j*theta) + self.rhoinv * exp(-1j*theta))
            w = (self.eta * 0.5 / self.n)  \
                * (self.rho * exp(1j*theta) - self.rhoinv * exp(-1j*theta))

        elif self.quadrule == 'ellipse_trapez_snug':

            if self.rhoinv <= 0:
                self._prnt('**** rhoinv = %g' % self.rhoinv)
                raise ValueError('Ellipse contour need rhoinv > 0 ' +
                                 '(or rho < infinity)')

            self._prnt('\nSpectralProj: Setting snug trapezoidal ' +
                       'rule on elliptical contour')
            theta = linspace(0, 2 * pi, num=n, endpoint=False)
            trho = 1 + pow(self.rho, self.n) + pow(self.rhoinv, self.n)
            a = pow(trho + np.sqrt(trho * trho - 1), 1.0 / self.n)
            self.eta = 2 * self.r / (a + (1.0 / a))
            z = self.c + self.eta * 0.5 \
                * (self.rho * exp(1j*theta) + self.rhoinv * exp(-1j*theta))
            w = (self.eta * 0.5 / self.n)  \
                * (self.rho * exp(1j*theta) - self.rhoinv * exp(-1j*theta))

        else:
            raise NotImplementedError(self.quad_err)

        return (z, w, n)

    def _reduce_quad_points(self):
        """
        Make these data members:

            self.z_reduced[i] = the i-th point where resolvent will be made.

            self.reflected[i] = true if z_reduced[i]'s reflection about
                the real axis is present in the given quadrature rule.

            self.w_reflect[i] = the quadrature weight of the reflection
                of z_reduced[i] whenever  reflected[i]=true.
        """

        keep = []
        real = abs(self.z.imag) < 1.e-13
        self.z_reduced = []
        self.reflected = []
        self.w_reflect = []

        for i, z in enumerate(self.z):
            if real[i]:
                keep.append(i)
                self.z_reduced.append(z)
                self.reflected.append(False)
                self.w_reflect.append(0)
            else:
                reflected = abs(z.conjugate() - self.z) / max(abs(self.z)) \
                    < 1.e-14
                ii = np.where(reflected)[0]
                if len(ii) == 1:
                    if ii[0] not in keep:
                        keep.append(i)
                        self.z_reduced.append(z)
                        self.reflected.append(True)
                        self.w_reflect.append(self.w[ii[0]])
                else:
                    raise ValueError('Check your quadrature rule!')

    def filter(self, zeta, setn=None):
        """ Return the value of the filter function

        f(zeta) = sum_k  w[k] / (z[k] - zeta)

        for all values input in the 1d numpy array zeta.
        """
        if setn is None:
            n = self.n
            z = self.z
            w = self.w
        else:
            z, w, n = self._compute_quadrature(setn)

        # Compute ZZ[j,k] = z[k] - zeta[j]
        ZZ = z - zeta[..., np.newaxis]
        # Try to protect against division by 0
        ZZ[np.where(abs(ZZ) < 1.e-14)] += 1.e-14
        # Set  ZZ[j,k] = 1.0  / (z[k] - zeta[j])
        ZZ = 1.0 / ZZ

        return (ZZ.dot(w), n)

    def plotfilter(self, numplotpts=200, setn=None):
        """ Plot the real component of the filter function

        f(zeta) = 1/n * sum_k (z[k] - c) / (z[k] - zeta).

        Note that f(A) is trapezoidal approximation to our spectral
        projector

        (2 pi i)^-1  int_C (z - A)^-1 dz

        using n equally spaced points on the circle C.  If you like to
        set n to a value N that is different from the number of points
        of this object's trapezoidal rule, then use setn=N.
        """

        zeta = np.linspace(self.c - 2 * self.r, self.c + 2 * self.r,
                           numplotpts)
        f, n = self.filter(zeta, setn)
        plt.plot(zeta, f.real)
        plt.title('Real part of filter with %d quadrature points' % n)
        plt.show()

    # Derived classes MUST implement the following methods #############

    def __mul__(self, y):
        """Action of the arroximate spectral projection operator P on a span.
        Returns P * y. Note that y may be overwritten and used as scratch.
        """
        raise NotImplementedError(self.err)

    def __rmul__(self, y):
        """Adjoint action of the approximate spectral projection operator's
        application on a span (derived classes must implement this).
        Right multiplication should be so implemented that y * P
        must return (yᴴ P)ᴴ = Pᴴ y, where ᴴ denotes Hermitian transpose.
        Note that y may be overwritten and used as scratch.
        """
        raise NotImplementedError(self.err)

    def rayleigh(self, q):
        """Compute (Aq,q) and (q,q) in the H-inner product (.,.) for each
        vector spanning q.  Expected return arguments are two m x m
        numpy arrays if q has m vectors. Input q will not be overwritten.
        """
        raise NotImplementedError(self.err)

    def rayleigh_nsa(self, ql, qr, qAq=not None, qBq=not None):
        """
        Return qAq[i, j] = (A qr[j],ql[i]) and
               qBq[i, j] = (qr[j], ql[i]),  in the H-inner product (.,.)
        unless they are declared None on input. If one is None, only
        the other is computed. Inputs ql, qr should not be overwritten.
        """
        raise NotImplementedError(self.err)

    def residual(self, lam, y, yl=None):
        """Return suitable norms of the residuals A y[i] - lam[i] * y[i],
        or when yl is not None,  the maximum of norms of the
        right residual  A y[i] - lam[i] * y[i] and the
        left residual A.H yl[i] - conj(lam[i]) * yl[i].
        """
        raise NotImplementedError(self.err)

    def allocate_feast_scratch_like(self, y):
        """Allocate scratch space similar in size to y.  This is called at the
        beginning of the feast algorithm.  By default nothing is done,
        but derived classes can overload to allocate scratch before iterations.
        """
        return

    # FEAST assuming the above methods are available #################

    def feast_hermitian_step(self, q, cut_tol=1.e-16):
        """Perform one step of the FEAST algorithm assuming that the
        operator A, and therefore its spectral projector P, is
        self-adjoint. The statement

        lambda, y = P.feast_hermitian_step(q)

        takes as input the span q and outputs a (possibly truncated
        according to 'cut_tol') span y that approximates the eigenspace
        and the corresponding eigenvalues lambda.
        """

        y = self * q  # Apply spectral projector
        yAy, yBy = self.rayleigh(y)  # Form Rayleigh-Ritz eigenproblem

        try:

            ew, ev = eigh(yAy, yBy)  # Small Hermitian eigenproblem

            if not np.isfinite(ew).all():
                raise LinAlgError(
                    'Is your B ok? eigh gave nonnumbers!' + 'ew = ',
                    np.array_str(ew))

        except (RuntimeError, RuntimeWarning, LinAlgError):

            self._prnt('   Reorthogonalizing and trying again...')

            y.orthoreduce(cut_tol)  # QR, truncate span, & try again
            yAy, yBy = self.rayleigh(y)  # Form Rayleigh-Ritz eigenproblem
            ew, ev = eig(yAy, yBy)  # Often eig is more robust than eigh

        y *= ev  # Recover large eigenvectors
        return (ew, y)

    def feast_nsa_biorth_step(self, yl, yr, cut_tol=1.e-16, eta_tol=1e-16):
        """
        Perform one step of the FEAST algorithm for NSA (non-selfadjoint)
        operators with biorthogonal left and right eigenvectors.
        INPUTS:
            yl - a span object for the left eigenspace
            yr - a span object for the right eigenspace
        OUTPUTS:
            ew - the current set of eigenvalue approximations
            yl, yr -  updated left and right eigenspaces
        """

        qr = self * yr
        ql = yl * self

        if id(ql) == id(qr):
            raise ValueError('Left & right products point to same memory!')

        self.removeker(ql, qr, eta_tol)
        lAr, lBr = self.rayleigh_nsa(ql, qr)

        try:
            lam, wl, wr = eig(lAr, lBr, left=True)

            if not np.isfinite(lam).all():
                raise LinAlgError('Is your B ok? eig gave nonnumbers!\n lam=' +
                                  np.array_str(lam))

        except (RuntimeError, RuntimeWarning, LinAlgError):

            # Use modified Gram Schmidt to reduce any linear dependence
            self._prnt('   Scipy small eig solver failed!')
            self._prnt('   Last ditch retry after mod-Gram-Schmidt ' +
                       'on left/right spans:')
            qr.orthoreduce(cut_tol)
            ql.orthoreduce(cut_tol)
            if qr.m != ql.m:
                m = min(ql.m, qr.m)
                qr.remove(list(range(m, qr.m)))
                ql.remove(list(range(m, ql.m)))

            lAr, lBr = self.rayleigh_nsa(ql, qr)
            lam, wl, wr = eig(lAr, lBr, left=True)

        ql *= wl
        qr *= wr

        return (lam, ql, qr)

    def feast_nsa_step(self, q, cut_tol=1.e-16):
        """
        Perform one step of a simple FEAST algorithm for NSA (non-selfadjoint)
        operators where modified Gram-Schmidt is used for right eigenvector
        orthogonalization and no left eigenvectors are computed.

        lamda, y = P.feast_nsa_step(q)

        """

        y = self * q  # Apply spectral projector
        y.orthoreduce(cut_tol)
        yAy, yBy = self.rayleigh(y)  # Form Rayleigh-Ritz eigenproblem

        ew, ev = eig(yAy, yBy)
        y *= ev

        # # A potential alternative to the above two lines, possibly better
        # # for non-normal matrices:
        # aa, bb, q, z = scipy.linalg.qz(yAy, yBy, output='complex')
        # ew = np.diag(aa) / np.diag(bb)

        return (ew, y)

    def feast(self,
              Y,
              Yl=None,
              hermitian=True,
              stop_tol=1.e-9,
              cut_tol=1.e-16,
              eta_tol=1e-16,
              nrestarts=10,
              niterations=30,
              check_contour=3,
              check_kpt_spurious=0,
              reportresiduals=False,
              exactewf=None):
        """
        An implementation of the FEAST algorithm:

        INPUTS:

        Y: Initial space (a Span object).

        hermitian: If True, then assume that the operator is Hermitian.

        Yl: If not None, then use biorthogonal FEAST iterations, and compute
        left eigenvectors, using  Yl for initial left eigenspace iterate.

        stop_tol: Stop when eigenvalues differ by this tolerance.

        cut_tol: Tolerance used in decisions to reduce span to match
        the eigenspace dimension.

        eta_tol: Tolerance used in decisions to reduce span to eliminate
        elements in the null space of rhs matrix.

        nrestarts: Number of times FEAST should restart and try again
        upon convergence failure.

        niterations: Number of FEAST iterations to do in each restart
        (unless convergence is reached).

        check_contour=N: If N>0, check if Ritz values are in the contour
        every N iterations. (No check if N=0.)

        check_kpt_spurious=N: If N>0, use [KPT]'s check to flag spurious
        ews every N iterations. (No check if N=0.)


        OUTPUTS:

        lam: Computed approximation of eigenvalues (a 1D numpy array).

        Y: Computed approximation of eigenspace (a span object).

        history =  ewerrors, eferrors, ewhdist, residuals, ews, converged
                ewerrors:  if exact eigenvalue given, this gives the
                           eigenvalue errors at each FEAST iteration,
                eferrors:  if exact eigenvalue given, this gives the
                           eigenfunction errors, computed by the virtual
                           function "compute_error" at each FEAST iteration,
                ewhdist:   hausdorff distance between successive FEAST
                           eigenvalue iterates,
                residuals: if reportresiduals=True, this gives norms
                           of the residual (A x - lam x), computed by the
                           virtual function "residual" at each iteration.
                ews:       eigenvalue approximants from each iteration.
                converged: True if stop_tol reached.
        """

        lam0 = 1.e100 * ones(Y.m)
        converged = False
        residuals = []
        ewerrors = []
        eferrors = []
        ewhdist = []
        ews = []
        biorth = Yl is not None

        if biorth and hermitian:
            raise ValueError('No need to compute left eigenvectors ' +
                             'in the Hermitian case!')
        if not hermitian and self.reduce_sym:
            raise ValueError('Refusing to do non-Hermitian feast ' +
                             'with symmetry reduction!')
        if nrestarts <= 0:
            nrestarts = 1

        self._prnt('\n=========== Starting FEAST iterations ===========')

        for restarts in range(1, nrestarts + 1):

            self._prnt('Trying with %d vectors:\n' % Y.m)
            self.allocate_feast_scratch_like(Y)

            for it in range(1, niterations + 1):
                msg = ' ITERATION %d ' % it
                if restarts > 1:
                    msg += ' restart %d, ' % (restarts - 1)
                msg += 'with %d vectors' % Y.m
                if biorth:
                    msg += ' and %d left vectors' % Yl.m
                self._prnt(msg)

                if hermitian is True:
                    lam, Y = self.feast_hermitian_step(Y, cut_tol)
                else:
                    if biorth:
                        lam, Yl, Y = \
                            self.feast_nsa_biorth_step(Yl, Y,
                                                       eta_tol=eta_tol,
                                                       cut_tol=cut_tol)
                    else:
                        lam, Y = self.feast_nsa_step(Y, cut_tol)
                ews.append(lam)

                if len(lam) == 0:
                    self._prnt('\n***No candidates left!')
                    break

                if reportresiduals is True:
                    residual = self.residual(lam, Y)
                    self._prnt('   Residuals:\n   ' + np.array_str(residual))
                    residuals.append(residual)

                # Sort the eigenvalue indices using lexicographic ordering
                # for complex numbers (first by real part, then imaginary)
                # for reporting them:
                sidx = np.argsort(lam)
                self._prnt('   Real part of computed eigenvalues:\n   ' +
                           np.array_str(lam[sidx].real))
                if np.max(abs(lam.imag)) > stop_tol:
                    self._prnt('   Imag part of computed eigenvalues:\n   ' +
                               np.array_str(lam[sidx].imag))

                # Compute the relative Hausdorff distance between successive
                # sets of eigenvalue approximations.
                hmat = np.abs(lam[:, np.newaxis] - lam0[np.newaxis, :])
                hminlam0 = np.min(hmat, axis=0) / self.ewchar
                hminlam = np.min(hmat, axis=1) / self.ewchar
                relhdist = max(np.max(hminlam0), np.max(hminlam))

                self._prnt('   Relative Hausdorff distance from' +
                           ' prior iterate: %.3e\n' % relhdist)
                ewhdist.append(relhdist)

                if exactewf is not None:
                    ewdiff, efdiff = self.compute_error(Y, lam, exactewf)
                    ewerrors.append(ewdiff)
                    eferrors.append(efdiff)
                if check_contour > 0:
                    if it % check_contour == 0:
                        lam = self.within_contour(lam, Y, Yl)
                if check_kpt_spurious > 0:
                    if it % check_kpt_spurious == 0:
                        self.kpt_spurious(lam, Yl, Y)

                lam0 = lam
                if relhdist < stop_tol:
                    self._prnt('\n***FEAST converged with:')
                    self._prnt('\tRelative Hausdorff distance ' +
                               '= %.3e' % relhdist)

                    converged = True
                    break

            if converged:
                break
            elif restarts < nrestarts:
                self._prnt('\nIncreasing span and retrying...')
                Y.augment()
                if Yl is not None:
                    Yl.augment()
            else:
                self._prnt('\n***FEAST did not converge!')
        self._prnt('\n============= FEAST iterations done =============')

        history = (ewerrors, eferrors, ewhdist, residuals, ews, converged)

        return (lam, Y, history, Yl)

    # Facilities for eigenspace truncation ###########################

    def within_contour(self, lam, Y, Yl=None):
        """
        Remove eigenvalues in "lam" that are outside the contour
        and reduce the span object Y by removing the corresponding
        vectors.
        """

        # Indices corresponding to what eigenvectors and eigenvalues to
        # remove (i.e. "cut") from the search subspace.
        cut = []

        inside = self.within(lam)

        for k in range(len(lam)):
            if ~inside[k]:
                cut.append(k)

        keep = list(set(range(len(lam))).difference(cut))

        if len(cut) and len(keep):
            self._prnt('   Removing ew#' + str(cut) +
                       ' not enclosed by contour')
            lam = lam[keep]
            Y.remove(cut)
            if Yl is not None:
                Yl.remove(cut)

        return lam

    def kpt_spurious(self, lam, Yl, Yr, mu=0.1):
        """
        A check suggested in p.S786 of [Kestyn, Polizzi, Tang, 2016] for
        removing spurious ews.

            Yl - a span object for the left eigenspace

            Yr - a span object for the right eigenspace

            mu - when should a filter value close to 0 be treated as 0 for
        the purposes of spurious ew detection.
        """

        _, qlBqr = self.rayleigh_nsa(Yl, Yr, qAq=None)
        rho, _ = self.filter(lam)
        sp_ind = np.where(np.abs((rho**2 - np.diag(qlBqr)) / rho**2) >= mu)[0]
        if len(sp_ind) > 0:
            self._prnt('   Removing ew#' + str(sp_ind) +
                       ' found by KPT spurious test')
            Yr.remove(sp_ind)
            Yl.remove(sp_ind)

    def removeker(self, Yl, Yr, eta_tol=1e-16):
        """
        Given
            Yl = a span object candidate for the left eigenspace,
            Yr = a span object candidate for the right eigenspace,
        compute an eigendecomposition of (B Yr, Yl) and use it to
        remove vectors in kernel of B from the span. Eigenvalues
        less than eta_tol in absolute value signal which vectors
        to remove. Also normalize so that diagonal entries of (B Yr, Yl)
        with the revised Yr, Yl are  close to one in absolute value.
        """

        _, qlBqr = self.rayleigh_nsa(Yl, Yr, qAq=None)
        g, vl, vr = eig(qlBqr, left=True)
        Yl *= vl
        Yr *= vr

        ag = abs(g)
        self._prnt('   During kernel cleaning abs(g):\n   ' +
                   np.array_str(ag, precision=1))

        cut = []
        keep = []
        for i in range(Yl.m):
            if ag[i] < eta_tol:
                cut.append(i)
            else:
                keep.append(i)
                Yl.scale(i, 1 / np.sqrt(ag[i]))
                Yr.scale(i, 1 / np.sqrt(ag[i]))

        if len(cut) and len(keep):
            self._prnt('   Removing ew#' + str(cut))
            Yl.remove(cut)
            Yr.remove(cut)

        else:
            # remove nothing, but scale to avoid
            # accidental convergence to 0 vectors:
            for i in range(Yr.m):
                Yl.scale(i, 1 / np.sqrt(ag[i]))
                Yr.scale(i, 1 / np.sqrt(ag[i]))
