import numpy as np


def segment_list(ic):
    """
    Segment a list "ic", like [2, 3, 4, 9, 11, 12, 13, 17, 18], into
    contiguous parts, like  [[2, 3, 4], [9], [11, 12, 13], [17, 18]].
    """

    if len(ic) > 0:
        dividers = np.zeros(len(ic))
        dividers[0] = 1

        for j in range(len(ic)-1):
            if ic[j+1] == ic[j]+1:
                dividers[j+1] = 0
            else:
                dividers[j+1] = 1

        clust = [[ic[0]]]
        for j in range(1, len(ic)):
            if dividers[j] == 0:
                clust[-1] += [ic[j]]
            else:
                clust += [[ic[j]]]
    else:
        clust = []

    return clust


def simple_multiple_zoom(c1, c2, ew, delta=None):
    """
    Given a list "ew" of numbers/eigenvalues lying in the real
    interval (c1, c2), identify those eigenvalues that are
    closer than "delta" and call them as multiple eigenvalues
    (since they presumably approximate an ew of multiplicity>1),
    while the remainder are considered simple eigenvalues.
    The function call

       s, m = simple_multiple_zoom(c1, c2, ew, delta)

    outputs dictionaries s and m, pertaining to the simple and multiple
    eigenvalues respectively, such that

       s['index'] = list of (single) indices into "ew" containing simple ews,
       m['index'] = list of arrays of indices, one array for each cluster
                    that is thought of a approximating a multiple eigenvalue,
       s['center'], m['center'],  s['radius'], m['radius']  give the
                    centers and radii of circular contours that contain
                    only a single simple ew or only a single cluster of
                    multiple eigenvalues.

    Thus, the i-th simple eigenvalue  ew[s['index'][i]] is enclosed in a
    circular contour of radius  s['radius'][i] and center s['center'][i].
    The i-th multiple eigenvalue cluster  [ew[l] for l in m['index'][i]]
    is enclosed in circle of radius m['radius][i] and center m['center'][i].
    """

    # Set default delta
    if delta is None:
        delta = (c2 - c1) / len(ew) / 10

    ind = np.argsort(ew)   # Do not sort/modify the input ew
    e = np.array(ew)       # Copy ew into np.array and sort
    e = e[ind]

    if c1 > e[0] or c2 < e[-1]:
        raise ValueError('List of numbers in ew not contained ' +
                         'in interval (c1, c2).')

    # Make e = [c1, e, c2]
    e = np.insert(e, 0, c1)
    e = np.insert(e, len(e), c2)

    # Make d[i] = e[i+1] - e[i]  for i = 0, 1,..., len(e)-1.
    d = e[1:] - e[:-1]

    if d[0] < delta:
        print('Warning: First ew element too close to c1!')
        d[0] = 2*delta
    if d[-1] < delta:
        print('Warning: Last ew element too close to c2!')
        d[-1] = 2*delta

    # Identify multiple eigenvalues
    ic = np.where(d < delta)[0]
    clus = segment_list(ic)

    clusters = []
    clusterindices = []
    centers = []
    radii = []

    for cind in clus:

        clusters.append(e[cind[0]:cind[-1]+2])
        clusterindices.append(np.arange(cind[0], cind[-1]+2)-1)

        # center set to mean of ews of this cluster
        centers.append(sum(clusters[-1])/len(clusters[-1]))

        # radius set to minimal distance to next ew or interval endpoints
        radii.append(0.5 * min(e[cind[0]] - e[cind[0]-1],
                               e[cind[-1]+2] - e[cind[-1]+1]))

    # The remaining are simple eigenvalues
    simplewindices = \
        np.setdiff1d(np.arange(len(ew)), np.concatenate(clusterindices)) \
        if len(clusterindices) > 0 else np.arange(len(ew))

    rad = []
    ctr = []
    for sind in simplewindices:
        ctr += [ew[sind]]
        rad += [0.5 * min(e[sind+1] - e[sind],
                          e[sind+2] - e[sind+1])]

    multiplewindices = []
    for c in clusterindices:
        multiplewindices.append(ind[c])

    # Prepare dictionaries for output
    multiple = {}
    multiple['index'] = multiplewindices
    multiple['radius'] = radii
    multiple['center'] = centers

    simple = {}
    simple['index'] = ind[simplewindices].tolist()
    simple['radius'] = rad
    simple['center'] = ctr

    return simple, multiple
