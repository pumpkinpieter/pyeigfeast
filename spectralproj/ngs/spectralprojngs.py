from ..spectralproj import Span, SpectralProj
import ngsolve as ng
import netgen.geom2d
import numpy as np
from scipy.sparse.linalg import norm
from scipy.sparse import coo_matrix


class NGvecs(Span):
    """Class of spans of n ngsolve BaseVectors (made from finite element
    GridFunctions).

    """

    def __init__(self, fe_space, m, M=None, verbose=True):
        """ INPUTS to constructor:

        m:  number of spanning vectors

        fe_space:  Finite element  space in which the vectors lie

        M: Gram (mass) matrix of fe_space's inner product. (If you need
        orthogonalization in a non-standard inner product, give the Gram
        matrix of that inner product here.)

        """
        self.fes = fe_space
        self.autoupdate = fe_space.autoupdate
        super().__init__(fe_space.ndof, m, verbose)
        gf = ng.GridFunction(fe_space, autoupdate=self.autoupdate)
        self._mv = ng.MultiVector(gf.vec, m)
        if M is None:
            self.M = ng.IdentityMatrix(self.fes.ndof, complex=True)
        else:
            self.M = M
        self.verbose = verbose

    def __len__(self):
        return len(self._mv)

    def __getitem__(self, i):
        gf = ng.GridFunction(self.fes, autoupdate=self.autoupdate)
        gf.vec.data = self._mv[i]
        return gf

    def __setitem__(self, i, item):
        self._mv[i] = item

    def setrandom(self, seed=None):
        """Put random values as coefficients in NGsolve's basis expansion of
        the grid functions. Impose 0 at dirichlet constrained nodes.
        """
        rng = np.random.default_rng(seed)
        self.fromnumpy(
            rng.random((self.n, self.m)) + 1j * rng.random((self.n, self.m)))
        return self

    def zerodata(self):  # zero all vectors
        for k in range(self.m):
            self._mv[k].FV().NumPy()[:] = 0
        return self

    def zerobdry(self):
        for k in range(self.m):
            self._mv[k].FV().NumPy()[~self.fes.FreeDofs()] = 0

    def tonumpy(self):  # convert ngsolve span -> numpy equivalent
        y = np.zeros((self.n, self.m), dtype=complex)
        for j in range(self.m):
            y[:, j] = self._mv[j].FV().NumPy()[:]
        return y

    def fromnumpy(self, y):  # copy numpy array -> to this ngsolve span
        """Import vectors from numpy array and zero out dirichlet nodes"""

        if y.shape[0] != self.n:
            raise ValueError(
                'Asked to import wrongly sized numpy array:' + 'y.shape=',
                y.shape, 'self.n=', self.n)
        m = y.shape[1]
        ybc = y.T * np.array(self.fes.FreeDofs())  # numpy's *, not matrix mult
        self.__init__(self.fes, m, self.M, self.verbose)
        for j in range(self.m):
            self._mv[j].FV().NumPy()[:] = ybc[j, :]
        return self

    def conj(self, Cv=None):
        """ Overwrite self with complex conjugate if Cv is None;
        otherwise set Cv to complex conjugate of self. """
        if Cv is None:
            for k in range(self.m):
                self._mv[k].FV().NumPy().imag *= -1
        else:
            for k in range(self.m):
                Cv._mv[k].FV().NumPy()[:] =  \
                    np.conj(self._mv[k].FV().NumPy())

    def copy(self, slice=None):  # deep copy
        """
        Z = Y.copy(slice)  makes a NGVecs object Z whose spanning vectors
        are those of Y in the order slice[0], slice[1], ...
        """
        if slice is not None:
            if len(slice) > self.m:
                raise ValueError(
                    'Index slice cannot have more than %d entries' % self.m)
        else:
            slice = range(self.m)

        c = NGvecs(self.fes, len(slice), M=self.M, verbose=self.verbose)
        for j in range(len(slice)):
            c._mv[j].data = self._mv[slice[j]]
        return c

    def gridfun(self, name='ngvec', i=None):
        """If i is None, return a multidim gridfunction with this NGvecs
        object data. Otherwise, return only the i-th grid function."""

        if i is None:
            gf = ng.GridFunction(self.fes,
                                 multidim=self.m,
                                 name=name,
                                 autoupdate=self.autoupdate)
            for i in range(self.m):
                gf.vecs[i].data = self._mv[i]
        else:
            gf = ng.GridFunction(self.fes,
                                 name=name,
                                 autoupdate=self.autoupdate)
            gf.vec.data = self._mv[i]
        return gf

    def draw(self, name='ngvec'):
        ng.Draw(self.gridfun(name))

    def apply(self, fun, name='result'):
        """ Apply an function "fun" to all gridfunctions of this span
        and return the result in another multdim gridfunction.
        (Depending on "fun", there will be a projection via Set.)  """

        # g = ng.GridFunction(self.fes, multidim=self.m, name=name)
        # selfgfun = self.gridfun()
        # for i in range(self.m):
        #     g.Set(fun(selfgfun.MDComponent(i)), mdcomp=i)

        g = ng.GridFunction(self.fes,
                            multidim=self.m,
                            name=name,
                            autoupdate=self.autoupdate)
        for i in range(self.m):
            selfgfun = self.gridfun(i=i)
            g.Set(fun(selfgfun, mdcomp=i))
        return g

    def applyfnl(self, fnl):
        """ Apply a functional "fnl" to all gridfunctions of this span
        and return the result in a list.  """

        vals = []
        selfgfun = self.gridfun()
        for i in range(self.m):
            vals.append(fnl(selfgfun.MDComponent(i)))
        return vals

    def scale(self, i, a):
        self._mv[i] *= a

    def normalize(self, work=None):
        """ Make each vector of the span have M-norm one. """

        nrms = []
        if work is None:
            work = self._mv[0].CreateVector()
        for i in range(self.m):
            work.data = self.M * self._mv[i]
            nrm = ng.sqrt(ng.InnerProduct(work, self._mv[i]).real)
            nrms.append(nrm)
        nrms = np.array(nrms)
        self._prnt('   Normalizing by M-norms: max=%.1e, min=%.1e' %
                   (max(nrms), min(nrms)))
        if min(nrms) < 1e-16:
            raise RuntimeWarning('*** Encountered 0-vector in span!')
        for i in range(self.m):
            self._mv[i] /= nrm

    def centernormalize(self, mip):
        """
        Make the real part of the eigenfunction at mesh point "mip" positive,
        if possible, and normalize.
        """

        w = ng.GridFunction(self.fes, autoupdate=self.autoupdate)
        for i in range(self.m):
            w.vec.data = self._mv[i]
            val = w(mip)
            if abs(val) > 1e-10 and val.real < 0:
                valconj = val.real - 1j * val.imag
                self._mv[i] *= valconj
        self.normalize(work=w.vec)

    def augment(self, mnew=None):
        """
        Increase m and reinitialize to a larger random span
        """

        # Increase m and reinitialize to a larger random span
        if mnew is None:
            if self.m == 0:
                mnew = 2
            else:
                mnew = 2 * self.m
        self._mv.Extend(mnew - self.m)
        # fill extended space  with random vectors
        d = mnew - self.m
        yrnd = np.random.rand(self.n, d) + 1j * np.random.rand(self.n, d)
        for j in range(self.m, mnew):
            self._mv[j].FV().NumPy()[:] = yrnd[:, j - self.m]
        self.m = mnew
        return self

    #  Virtual functions for FEAST ##############################

    def remove(self, cut):
        """Remove vectors with cut indices
        """
        if len(cut) == 0:
            return
        keep = list(set(range(self.m)).difference(cut))
        self._mv = self._mv[keep]
        self.m = self.m - len(cut)

    def orthoreduce(self, tol=1.e-9):
        """ Orthogonalize the basis vectors of the span and throw away
        vectors that are almost linearly dependent.
        """

        # r, _ = self.modifiedGScolpivot(tol=tol)

        r = self._mv.Orthogonalize(ipmat=self.M)

        self._prnt('   QR found abs(diag(R)):\n   ' +
                   np.array_str(abs(np.diag(r)), precision=1))
        cut = []
        for i in range(self.m):
            if np.abs(r[i, i]) < tol:
                cut.append(i)
        if len(cut) and len(cut) < self.m - 1:
            self._prnt('   Removing %d vector(s).' % len(cut))
            self.remove(cut)

    def modifiedGScolpivot(self, M=None, tol=1.e-9):
        """ Orthogonalize by modified Gram Schmidt in M-innner product
        with column pivoting.
        """

        if M is None:
            M = self.M
        tmp = self._mv[0].CreateVector()
        r = np.zeros((self.m, self.m), dtype=complex)
        swaps = []

        for i in range(self.m):

            nrms = []
            for j in range(i, self.m):
                tmp.data = M * self._mv[j]
                nrms.append(abs(ng.InnerProduct(tmp, self._mv[j])))
            maxind = np.argmax(nrms) + i

            # Column pivoting
            if maxind != i:
                swaps.append((i, maxind))
                self._mv.Replace([i, maxind], self._mv[(maxind, i)])
                r[:i, i], r[:i, maxind] = r[:i, maxind], r[:i, i].copy()

            # r[i,i] = M-norm of y[i]
            tmp.data = M * self._mv[i]
            r[i, i] = np.sqrt(ng.InnerProduct(tmp, self._mv[i]))

            if abs(r[i, i]) < tol:
                continue

            self._mv[i] *= complex(1.0 / r[i, i])  # q[i] = y[i] / r[i,i]
            tmp *= complex(1.0 / r[i, i])  # tmp = M * q[i]

            for j in range(i + 1, self.m):

                # r[i,j] = (y[j], M * q[i])
                r[i, j] = ng.InnerProduct(self._mv[j], tmp)

                # y[j] = y[j] - r[i,j] * q[i]
                self._mv[j].data -= complex(r[i, j]) * self._mv[i]

        return r, swaps

    def __imul__(self, other):  # y *= u  where u is n x k or scalar

        if isinstance(other, (int, float, complex)):
            for k in range(self.m):  # scalar multiply
                self._mv[k].data *= other

        elif isinstance(other, np.ndarray):

            w = self.zeroclone()

            for k in range(other.shape[1]):
                #   w[:,k] = sum_j  y[:,j] * u[j,k]
                for j in range(self.m):
                    w._mv[k].data += complex(other[j, k]) * self._mv[j]

            for k in range(other.shape[1]):  # y = w[:k]
                self._mv[k].data = w._mv[k]
            self.m = other.shape[1]

        else:
            raise NotImplementedError()

        return self

    def __iadd__(self, other):
        """self += other """
        if isinstance(other, NGvecs):
            for k in range(self.m):
                self._mv[k].data += other._mv[k]
        else:
            raise NotImplementedError()
        return self

    def create(self):
        return NGvecs(self.fes, self.m, self.M, self.verbose)

    def zeroclone(self):  # create new memory and zero
        return self.create().zerodata()


####################################################################


class SpectralProjNG(SpectralProj):
    """A class of finite element approximations of spectral projections

        P = (2 π i)⁻¹ ∮ (z - 𝒜)⁻¹  dz

    where 𝒜 is (possibly non-selfadjoint) partial differential operator
    whose eigenvalues λ are sought using a weak formulation
        (𝒜 u, v) = λ (u, v)
    with an H-inner product (. , .) whose Gram matrix must be provided.
    """

    def __init__(self, fe_space, A, B, checks=True, **kwargs):
        """
        Construct the spectral projector of a possibly non-selfadjoint
        operator 𝒜 using stiffness matrices on some finite element space.

        INPUTS:

        * fe_space: Space with basis { phi[i] } and inner product (., .)_H
          Example: Lagrange finite element space with (u, v)_H = ∫ u v

        * A: Matrix with entries A[i, j] = (𝒜 phi[j], phi[i])_H
          Example: 𝒜 = -Δ with Dirichlet bc.  Then A[i, j] = ∫ ∇ϕⱼ · ∇ϕᵢ

        * B: Matrix with entries B[i, j] = (phi[j], phi[i])_H, so that
          B is the Gram matrix of the H-inner product.
          Example: For Lagrange FE space, B[i, j] = ∫ ϕⱼ ϕᵢ

        The remaining kwargs are passed to the base class, where
        they are documented.
        """

        self.fes = fe_space
        self.A = A
        self.B = B
        super().__init__(**kwargs)
        if checks:
            self.check_sym_innerprod()

        if self.reduce_sym:
            self._reduce_quad_points()
        else:
            self.z_reduced = self.z
            self.reflected = [False] * len(self.z)
            self.w_reflect = []

        if self.inverse is not None and checks:
            self.check_ngsinvH(self.inverse)
        self.make_resolvents()

        # scratch space
        self.scratch = ng.GridFunction(self.fes,
                                       autoupdate=self.fes.autoupdate)
        self.scratch2 = ng.GridFunction(self.fes,
                                        autoupdate=self.fes.autoupdate)
        # scratch space set later by allocate_feast_scratch_like(NGVec object)
        self.lmulscratch = None  # Two lists of NGvec scratch objects
        self.rmulscratch = None  # used in mul and rmul (only).
        self.rayleighscratch = None  # Used only in rayleigh and rayleigh_nsa.

    def make_resolvents(self):
        """Make   self.R[i] = inv( z[i] * B - A ).

        Note that the stiffness matrix representation of the resolvent
        operator at z[i] is NOT self.R[i], but equals the above
        matrix multiplied by B, i.e., (z[i] * B - A)⁻¹ * B, since B
        generates the inner product. We store R[i] instead of the actual
        resolvent to exploit some savings by pre-applying B.
        """

        defaultinvtype = self.A.GetInverseType()
        if self.inverse is None:
            useinvtype = defaultinvtype
        else:
            useinvtype = self.inverse
        self._prnt('\nSpectralProjNG: ' +
                   'Computing resolvents using %s' % useinvtype)
        self.R = []
        for z in self.z_reduced:
            self._prnt('SpectralProjNG:   ' +
                       'Factorizing at z = %+7.3f% + 7.3fj' % (z.real, z.imag))
            with ng.TaskManager():
                r = self.A.CreateMatrix()
                r.AsVector().data = complex(z) * self.B.AsVector() - \
                    self.A.AsVector()
                rinv = r.Inverse(self.fes.FreeDofs(), inverse=useinvtype)

            self.R.append(rinv)

    def apply_resolvent(self, i, y, Ry):
        """
        INPUT:  BaseVector y
        OUTPUT: BaseVector Ry = inv( z[i] * B - A ) * B * y
                              = R[i] * B * y
        """
        By = self.scratch.vec
        By.data = self.B * y
        Ry.data = self.R[i] * By

    def apply_resolvent_adj(self, i, y, x):
        """
        INPUT:  BaseVector y
        OUTPUT: BaseVector x = inv(z[i] * B - A).H * B * y
                             = R[i].H * B * y
        """
        By = self.scratch.vec
        By.data = self.B * y
        x.data = self.R[i].H * By

    def setkrylov(self, Y, r):
        """Set span y to the Krylov space spanned by

        B^-1 * r,  B^-1 * A * B^-1 * r,  B^-1 * A^2 * B^-1 * r, ...

        """

        Y._mv[0].data = self.B.Inverse(self.fes.FreeDofs()) * r
        for j in range(1, Y.m):
            Y._mv[j].data = self.A * Y._mv[j - 1]
            Y._mv[j].data = self.B.Inverse(self.fes.FreeDofs()) * Y._mv[j]

    def matrix_filter(self):
        """
        Return the matrix approximation S_N^h of the exact spectral
        projector S, as a dense numpy matrix. (Memory intensive!)
        Only the restriction of the matrix to free dofs is returned.
        """

        ndof = self.fes.ndof
        y = np.identity(ndof, dtype=complex)
        Y = NGvecs(self.fes, ndof, self.B)
        Y.fromnumpy(y)
        PY = self * Y
        S = PY.tonumpy()

        free = np.array(self.fes.FreeDofs())
        freedofs = np.where(free)[0]

        SS = S[:, freedofs]
        S = SS[freedofs, :]

        return S

    def check_ngsinvH(self, inversetype):
        """
        This test is prompted by a comment in NGSolve that inverse.H is
        a work in progress. For umfpack and pardiso sparse inverses
        we checked it works, but if user gives another inverse type, this
        check may be useful.
        """
        self._prnt('SpectralProjNG: Checking if inverse.H works for %s' %
                   inversetype)

        ngm0 = netgen.geom2d.unit_square.GenerateMesh(maxh=0.49)
        mesh0 = ng.Mesh(ngm0)
        X = ng.H1(mesh0, order=2, dirichlet='left', complex=True)
        u, v = X.TnT()
        a = ng.BilinearForm(X)
        a += (1 + 1j) * ng.grad(u) * ng.grad(v) * ng.dx
        with ng.TaskManager():
            a.Assemble()

        # Set v, w so that A.H * w = v

        v = ng.GridFunction(X, autoupdate=X.autoupdate).vec
        w = v.CreateVector()
        w.SetRandom(seed=1)
        w.FV().NumPy()[~X.FreeDofs()] = 0
        v.data = a.mat.H * w

        w2 = v.CreateVector()
        e = v.CreateVector()

        # Try recovering w by a.Inverse.H

        ainv = a.mat.Inverse(X.FreeDofs(), inverse=inversetype)
        w2.data = ainv.H * v
        e.data = w - w2
        err = ng.Norm(e)

        if err > 1e-14:
            self._prnt('*** This should be zero: ||e||=%g' % err)
            raise RuntimeError('Requested inverse %s does .H incorrectly' %
                               inversetype)

    def check_sym_innerprod(self):

        i, j, bval = self.B.COO()
        b = coo_matrix((bval, (i, j)), dtype=np.complex128)
        normb = norm(b, 'fro')
        if normb < 1e-16:
            raise ValueError('B is too close to 0')
        d = norm(b - b.H, 'fro') / normb
        if d > 1.e-15:
            raise ValueError('B does not generate an inner product!\n' +
                             'Use SpectralProjNGGeneral instead.')

    def get_mul_scratch(self, v, noteq=None, right=False):
        """ Get a scratch span that is like "v" but not equal to v.
        If noteq is also given, then ensure that the scratch span returned
        has neither the same memory as "noteq" nor that of "v".
        If right=True, then choose the scratch memory from those
        allocated for right multiplication; otherwise from those allocated
        for left multiplication."""

        if self.rmulscratch is None or self.lmulscratch is None:
            self.allocate_feast_scratch_like(v)

        if right is True:
            scratchlist = self.rmulscratch
        else:
            scratchlist = self.lmulscratch

        notv = [s for s in scratchlist if id(s) != id(v)]
        if noteq is not None:
            notv = [s for s in notv if id(s) != id(noteq)]
        if len(notv):
            scratch = notv[0]
        else:
            scratchlist.append(v.create())
            scratch = scratchlist[-1]

        if v.m > scratch.m:
            scratch = v.create()
            return scratch
        else:
            if v.m < scratch.m:
                cut = [i for i in range(v.m, scratch.m)]
                scratch.remove(cut)
                return scratch
            else:
                return scratch

    #  Virtual functions needed for FEAST ##########################

    def __mul__(self, v):
        """ Return P * v (the action of spectral projector P on span v) """

        Bv = self.get_mul_scratch(v)
        Pv = self.get_mul_scratch(v, noteq=Bv)
        tmp = self.scratch.vec
        tmp2 = self.scratch2.vec

        with ng.TaskManager():

            Pv.zerodata()

            for k in range(v.m):
                Bv._mv[k].data = self.B * v._mv[k]

            for i in range(len(self.z_reduced)):

                for k in range(v.m):

                    # Compute  R(z) v[k] = inv(zB - A) B v[k]:
                    tmp.data = self.R[i] * Bv._mv[k]
                    Pv._mv[k].data += complex(self.w[i]) * tmp

                    # If reflection of this quadrature node is included,
                    # then, we must also add contribution due to conj(z),
                    # equaling = R(conj(z)) v[k] = conj(R(z) conj(v[k])):
                    # = conj( inv(zB - A) conj(B v[k]) ):

                    if self.reflected[i]:

                        # tmp <- conj(B v[k])
                        tmp.FV().NumPy()[:]\
                            = np.conjugate(Bv._mv[k].FV().NumPy())
                        # tmp2 <- inv(zB - A) * conj(B v[k])
                        tmp2.data = self.R[i] * tmp
                        # tmp <- conj(inv(zB - A) * conj(B v[k]))
                        tmp.FV().NumPy()[:]\
                            = np.conjugate(tmp2.FV().NumPy())
                        # Accumulate in Pv
                        Pv._mv[k].data += complex(self.w_reflect[i]) * tmp

        return Pv

    def __rmul__(self, v):
        """
        Call v * P to get (vᴴ P)ᴴ = Pᴴ v, where ᴴ denotes the adjoint with
        respect to the H-inner product.
        """

        if self.reduce_sym:
            raise NotImplementedError('Adjoint not implemented for ' +
                                      'case reduce_sym=True')

        PHv = self.get_mul_scratch(v, right=True)
        Bv = self.get_mul_scratch(v, noteq=PHv, right=True)
        tmp2 = self.scratch2.vec

        with ng.TaskManager():

            PHv.zerodata()
            for k in range(v.m):
                Bv._mv[k].data = self.B * v._mv[k]

            for i in range(len(self.z)):

                for k in range(v.m):

                    # The adjoint of
                    #    Resolvent(z) = (zB - A)⁻¹ * B = R(z) * B
                    # with respect to the H-inner product generated by B is
                    #    adjoint( Resolvent(z) ) = R(z).H * B
                    # This formula is implemented below. Note that it is
                    # correct only if B generates the H-inner product.

                    tmp2.data = self.R[i].H * Bv._mv[k]

                    PHv._mv[k].data += complex(np.conjugate(self.w[i])) * tmp2

                    # We assume that this adjoint is needed only in
                    # the nonselfadjoint case. Hence no reflected
                    # quadrature nodes to exploit symmetry is expected here.

        return PHv

    def rayleigh(self, q):
        """
        Return qAq[i, j] = (𝒜 qr[j], ql[i])_H and
               qBq[i, j] = (qr[j], ql[i])_H,  in the H-inner product,
        Returns two m x m numpy arrays if q has m vectors,
        the second of which must be Hermitian.
        """

        q = q._mv  # just renaming for readability
        Aq = self.rayleighscratch._mv[:len(q)]
        Bq = self.rayleighscratch._mv[:len(q)]

        Aq.data = self.A * q  # compute by MultiVector InnerProduct
        # qAq = ng.InnerProduct(Aq, q).NumPy().T
        qAq = ng.InnerProduct(Aq, q).NumPy()

        Bq.data = self.B * q
        # qBq = ng.InnerProduct(Bq, q).NumPy().T
        qBq = ng.InnerProduct(Bq, q).NumPy()

        return (qAq, qBq)

    def rayleigh_nsa(self, ql, qr, qAq=not None, qBq=not None):
        """
        Return qAq[i, j] = (𝒜 qr[j], ql[i])_H and
               qBq[i, j] = (qr[j], ql[i])_H,  in the H-inner product,
        unless they are declared None on input. If one is None, only
        the other is computed.
        """

        qr = qr._mv  # renaming for readability
        ql = ql._mv
        Aqr = self.rayleighscratch._mv[:len(qr)]
        Bqr = self.rayleighscratch._mv[:len(qr)]

        if qAq is not None:  # compute by MultiVector InnerProduct

            Aqr.data = self.A * qr
            # qAq = ng.InnerProduct(Aqr, ql).NumPy().T
            qAq = ng.InnerProduct(Aqr, ql).NumPy()

        if qBq is not None:

            Bqr.data = self.B * qr
            # qBq = ng.InnerProduct(Bqr, ql).NumPy().T
            qBq = ng.InnerProduct(Bqr, ql).NumPy()

        return (qAq, qBq)

    def residual(self, lam, y):
        """
        Return the inv(A)-norm of residuals A * y[i] - lam[i] * B * y[i]
        """

        residnorms = np.zeros(y.m)
        residual = self.scratch.vec
        residAinv = residual.CreateVector()

        for j in range(y.m):

            residual.data = self.B * y._mv[j]
            nrm = ng.InnerProduct(y._mv[j], residual)

            residual.data = complex(-lam[j]) * residual
            residual.data += self.A * y._mv[j]

            residAinv.data = self.A.Inverse(self.fes.FreeDofs()) * residual

            # Compute || A * y[l] - lam[i] * B * y[l] ||_A^-1
            residnorms[j] = np.sqrt(
                abs(ng.InnerProduct(residual, residAinv) / nrm))

            # NOTE: If computing residual in L2-norm (not in Ainv-norm
            # as above), be sure to ZERO OUT non-FreeDofs!

        return residnorms

    def allocate_feast_scratch_like(self, y):
        """Allocate scratch space used (only) by feast algorithm.
        """
        self.lmulscratch = [y.create(), y.create(), y.create()]
        self.rmulscratch = [y.create(), y.create(), y.create()]
        self.rayleighscratch = y.create()


####################################################################


class SpectralProjNGR(SpectralProj):
    """
    A class to approximate  P = (2 π i)⁻¹ ∮ R(z) dz  where the resolvent
    R(z) = (z - 𝒜)⁻¹ is given by another class.
    """

    def __init__(self, makeresolvent, **kwargs):
        """
        OUTPUT an approximation of P = (2 π i)⁻¹ ∮ R(z) dz.

        INPUT a function makeresolvent(z). For any complex number z,
        the resolvent object r = makeresolvent(z) should have the
        following methods:
            * r.act(v, Rv)  -> Given NGvec v, compute Rv = R(z) * v
            * r.adj(v, Rv)  -> Given NGvec v, compute Rv = R(z).H * v.
            *.r.rayleigh_nsa, r.rayleigh.
        """

        super().__init__(**kwargs)
        if self.reduce_sym:
            self._reduce_quad_points()
        else:
            self.z_reduced = self.z
            self.reflected = [False] * len(self.z)
            self.w_reflect = []

        self.resolvents = []
        for z in self.z_reduced:
            self._prnt('SpectralProjNGR:   ' +
                       'Making resolvent at z = %+7.3f% + 7.3fj' %
                       (z.real, z.imag))
            self.resolvents.append(makeresolvent(z))
        self.scratch = None
        self.rscratch = None
        self.scratchconj = None

    def allocate_feast_scratch_like(self, v):
        """Allocate scratch space used (only) by feast algorithm. """
        self.scratch = [v.create(), v.create(), v.create(), v.create()]
        if self.reduce_sym:
            self.scratchconj = v.create()
        self.rscratch = [v.create(), v.create(), v.create(), v.create()]

    def getscratchlike(self, v, neq=None, rmul=False):
        """
        Get a list of scratch spans that is like "v" but not equal to v.
        If noteq is also given, then ensure that the scratch span returned
        has neither the same memory as "noteq" nor that of "v".
        """

        makescratch = False
        if self.scratch is not None:
            ss = self.scratch + self.rscratch
            if self.scratchconj is not None:
                ss += [self.scratchconj]
            for s in ss:
                if s.m < v.m:
                    makescratch = True
                if s.m > v.m:
                    s.remove([i for i in range(v.m, s.m)])
        else:
            makescratch = True

        if makescratch:
            self.allocate_feast_scratch_like(v)

        if rmul:
            nonv = [s for s in self.rscratch if id(s) != id(v)]
        else:
            nonv = [s for s in self.scratch if id(s) != id(v)]
        if neq is not None:
            nonv = [s for s in nonv if id(s) != id(neq)]
        return nonv[:min(3, len(nonv))]

    def __mul__(self, v):
        """ Return P * v (the action of this spectral projector P on
        NGvec object  v) """

        Pv, Rv, wrk = self.getscratchlike(v)
        Cv = self.scratchconj
        Pv.zerodata()

        for i in range(len(self.z_reduced)):

            self.resolvents[i].act(v, Rv, workspace=wrk)
            Pv._mv[:] += complex(self.w[i]) * Rv._mv

            # If reflection of this quadrature node is included,
            # then, add contribution due to conj(z), assuming
            #    R(conj(z)) v[k] = conj(R(z) conj(v[k])),
            # which holds, e.g., for resolvents of real matrices.

            if self.reflected[i]:
                v.conj(Cv)
                self.resolvents[i].act(Cv, Rv, workspace=wrk)
                Rv.conj()
                Pv._mv[:] += complex(self.w_reflect[i]) * Rv._mv

        return Pv

    def __rmul__(self, v):
        """
        Call v * P to compute (vᴴ P)ᴴ = Pᴴ v, where ᴴ denotes the adjoint with
        respect to the H-inner product.
        """

        if self.reduce_sym:
            # We assume that this adjoint is needed only in
            # the nonselfadjoint case, hence no facility for reflected
            # quadrature nodes to exploit symmetry is planned for:
            raise NotImplementedError('Adjoint not implemented for ' +
                                      'case reduce_sym=True')
        PHv, RHv, wrk = self.getscratchlike(v, rmul=True)
        PHv.zerodata()

        for i in range(len(self.z)):  # PHv += conj(w[i]) * (R.H[i] * v)
            self.resolvents[i].adj(v, RHv, workspace=wrk)
            PHv._mv[:] += complex(np.conjugate(self.w[i])) * RHv._mv

        return PHv

    def rayleigh(self, q):
        """
        Return qAq[i, j] = (𝒜 qr[j], ql[i])_H and
               qBq[i, j] = (qr[j], ql[i])_H,  in the H-inner product,
        Returns two m x m numpy arrays if q has m vectors,
        the second of which must be Hermitian.
        """
        wrk = self.getscratchlike(q)[0]
        return self.resolvents[0].rayleigh(q, workspace=wrk)

    def rayleigh_nsa(self, ql, qr, qAq=not None, qBq=not None):
        """
        Return qAq[i, j] = (𝒜 qr[j], ql[i])_H and
               qBq[i, j] = (qr[j], ql[i])_H, in the H-inner product,
        unless they are declared None on input. If one is None, only
        the other is computed.
        """
        wrk = self.getscratchlike(ql, neq=qr)[0]
        return self.resolvents[0].rayleigh_nsa(ql, qr, qAq, qBq, workspace=wrk)

    def residual(self, lam, y, yl=None):
        """Return suitable norms of the residuals A y[i] - lam[i] * y[i],
        or when yl is not None,  the maximum of norms of the
        right residual  A y[i] - lam[i] * y[i] and the
        left residual A.H yl[i] - conj(lam[i]) * yl[i].
        """
        return self.resolvents[0].residual(lam, y, yl)


####################################################################


class ResolventAB():
    """
    For eigenvalue problems of the form A x = λ B x, the resolvent is
    R(z) = (z - B⁻¹A)⁻¹ = (zB - A)⁻¹ B.  This class provides such
    resolvents assuming that A and B are given as NGSolve BaseMatrices
    and that B is a s.p.d matrix generating the inner product to be
    used for orthogonalization in the Rayleigh Ritz procedure.

    (This class, together with SpectralProjNGR, can be used to provide
    an alternate implementation of SpectralProjNG, but note that
    SpectralProjNG is able to make use of further optimizations.)
    """

    def __init__(self, fes, A, B, z):
        self.A = A
        self.B = B
        self.z = z
        self.fes = fes
        with ng.TaskManager():
            r = self.A.CreateMatrix()
            r.AsVector().data\
                = complex(z) * self.B.AsVector() - self.A.AsVector()
            self.rinv = r.Inverse(self.fes.FreeDofs())

    def act(self, v, Rv, workspace=None):
        if workspace is None:
            Bv = ng.MultiVector(v._mv[0], v.m)
        else:
            Bv = workspace._mv[:v.m]
        with ng.TaskManager():
            Bv[:] = self.B * v._mv
            Rv._mv[:] = self.rinv * Bv
            Rv.zerobdry()

    def adj(self, v, RHv, workspace=None):
        if workspace is None:
            Bv = ng.MultiVector(v._mv[0], v.m)
        else:
            Bv = workspace._mv[:v.m]
        with ng.TaskManager():
            Bv[:] = self.B * v._mv
            RHv._mv[:] = self.rinv.H * Bv
            RHv.zerobdry()

    def rayleigh_nsa(self, ql, qr, qAq=not None, qBq=not None, workspace=None):
        """
        Return qAq[i, j] = (𝒜 qr[j], ql[i])_H and
               qBq[i, j] = (qr[j], ql[i])_H,  in the H-inner product,
        unless they are declared None on input. If one is None, only
        the other is computed.
        """
        if workspace is None:
            Aqr = ng.MultiVector(qr._mv[0], qr.m)
        else:
            Aqr = workspace._mv[:qr.m]

        if qAq is not None:
            Aqr[:] = self.A * qr._mv
            # qAq = ng.InnerProduct(Aqr, ql._mv).NumPy().T
            qAq = ng.InnerProduct(Aqr, ql._mv).NumPy()

        if qBq is not None:
            Bqr = Aqr
            Bqr[:] = self.B * qr._mv
            # qBq = ng.InnerProduct(Bqr, ql._mv).NumPy().T
            qBq = ng.InnerProduct(Bqr, ql._mv).NumPy()

        return (qAq, qBq)

    def rayleigh(self, q, workspace=None):
        """
        Return qAq[i, j] = (𝒜 qr[j], ql[i])_H and
               qBq[i, j] = (qr[j], ql[i])_H,  in the H-inner product."""

        return self.rayleigh_nsa(q, q, workspace=workspace)
