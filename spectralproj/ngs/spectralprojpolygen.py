import numpy as np
import ngsolve as ng
from pyeigfeast.spectralproj.ngs import SpectralProjNGGeneral, NGvecs
from scipy.sparse.linalg import norm
from scipy.sparse import coo_matrix, bmat, eye
from ngsolve import BlockMatrix, BlockVector


class SpectralProjNGPolyGeneral(SpectralProjNGGeneral):

    """This class implements the polynomial feast eigensolver found in
    https://doi.org/10.1016/j.wavemoti.2021.102826

    It solves the polynomial eigenproblem of finding a λ and a
    nontrivial x satisfying A(λ) x = 0, where
             A(λ) = A[0] + λ A[1] + λ² A[2] + ⋯ + λᵈ A[d]
    is a matrix polynomial in λ.

    * The A[i] should be given to the constructor as ndof x ndof
    sparse matrix of NGSolve's BaseMatrix type and are stored as data
    member
        SpectralProjNGPoly.As[i].mat    (ndof x ndof BaseMatrix).

    * Evaluated matrix polynomial A(z) for any complex number z is given by
        SpectralProjNGPoly.polA(z)

    * The polynomial eigenproblem is converted to the larger d*ndof x d*ndof
    first companion linear eigenproblem
        AA xx = λ BB xx,                (d*ndof x d*ndof)
    which is then tailored for application of the FEAST algorithm. Here

    AA = [  0  I  0  .... 0   ]     BB = [  I  0  .....  0  ]
         [  0  0  I       :   ]          [  0  I         :  ]
         [  :     .   .   :   ]          [  :     .      :  ]
         [  0         .   I   ]          [  :       I    0  ]
         [A[0] A[1] ... A[d-1]]          [  0 ....  0  -A[d]]

    where I is a mass matrix given as input. By default I is identity.

    * The larger matrices AA and BB are available as data attributes

        SpectralProjNGPoly.BlockA,
        SpectralProjNGPoly.BlockB       (d x d BlockMatrix)

    both of the NGSolve type BlockMatrix. Each block is ndof x ndof.

    """

    # NOTE: This code often refers to "long" and "short" vectors, both
    # of which are of type BaseVector. The short ones are of length
    # ndof, while the long ones are of length ndof * d.

    def __init__(self, matrix_pol_coefs, fe_space,
                 radius, center, npts,
                 **kwargs):
        """
        Constructs A(λ) = A[0] + λ A[1] + λ² A[2] + ⋯ λᵈ A[d] and
        its spectral projector assuming that -A[d] generates
        an inner product (or at least a semi-inner product).

        INPUTS:
        * matrix_pol_coefs: list of matrix polynomial coefficients A[i]
          given as previously assembled NGSolve BilinearForm objects.

        * fe_space: space where A[i] acts (with ndof degrees of freedom).

        * Remaining inputs and kwargs are as documented in the base
          class SpectralProjNG.
        """

        self.As = matrix_pol_coefs

        self.degree = len(self.As) - 1    # This equals the "d" in comments
        if self.degree <= 1:
            raise ValueError('Not a polynomial eigenproblem! ' +
                             'len(matrix_pol_coefs)=%d' % self.degree)
        if kwargs.get('reduce_sym') is True:  # if None, give no error
            raise NotImplementedError('SpectralProjNGPoly: ' +
                                      'reduce_sym unimplemented')

        self.fes = fe_space

        defaultinvtype = self.As[0].mat.GetInverseType()
        if 'inverse' not in kwargs:
            kwargs['inverse'] = defaultinvtype
        else:
            if kwargs['inverse'] is None:
                kwargs['inverse'] = defaultinvtype
        super().__init__(fe_space, None, None,
                         radius=radius, center=center, npts=npts,
                         **kwargs)

        # Large matrices (sharing the same memory as input matrices)
        self.BlockA = self.AA()
        self.BlockB = self.BB()

        # scratch storage
        short = ng.GridFunction(self.fes)
        self.applyRscratch = [short.vec.CreateVector(),
                              short.vec.CreateVector(),
                              short.vec.CreateVector()]
        productspace = ng.FESpace([self.fes] * self.degree)
        self.longscratch = ng.GridFunction(productspace)

    def polA(self, z):
        """ Return the evaluated matrix polynomial A(z) at point z """

        with ng.TaskManager():
            Az = self.As[0].mat.CreateMatrix()
            Az.AsVector().data = self.As[0].mat.AsVector()
            for i in range(1, self.degree+1):
                Az.AsVector().data \
                    += complex(z**i) * self.As[i].mat.AsVector()
        return Az

    def AA(self):
        """ Return the d*ndof x d*ndof AA in the documentation """
        rows = []
        for i in range(self.degree-1):
            row = [None] * self.degree
            row[i+1] = ng.IdentityMatrix(self.fes.ndof, complex=True)
            rows.append(row)
        rows.append([self.As[i].mat for i in range(self.degree)])
        return BlockMatrix(rows)

    def BB(self):
        """ Return the BB in the linearization (d*ndof x d*ndof) """
        rows = []
        for i in range(self.degree-1):
            row = [None] * self.degree
            row[i] = ng.IdentityMatrix(self.fes.ndof, complex=True)
            rows.append(row)
        lastrow = [None] * self.degree
        lastrow[self.degree-1] = -self.As[self.degree].mat
        rows.append(lastrow)
        return BlockMatrix(rows)

    def applyAA(self, xx, AAxx):
        """ For a long vector xx, return AA * xx in long vector AAxx."""

        xxb = self.long2block(xx)
        AAxxb = self.long2block(AAxx)
        AAxxb.data = self.BlockA * xxb

    def applyBB(self, xx, BBxx):
        """ For a long vector xx, return BB * xx in long vector BBxx."""

        xxb = self.long2block(xx)
        BBxxb = self.long2block(BBxx)
        BBxxb.data = self.BlockB * xxb

    def scipyAs(self):
        """ Return list of coefficient matrices in scipy sparse format """

        As = []
        for k in range(len(self.As)):
            i, j, a = self.As[k].mat.COO()
            As.append(coo_matrix((a, (i, j))), dtype=np.complex128)
            Id = eye(self.fes.ndof)
        return As, Id

    def scipyBlockAB(self):
        """ Return BlockA and BlockB as scipy sparse matrices """

        As, Id = self.scipyAs()
        rows = []
        for i in range(self.degree-1):
            row = [None] * self.degree
            row[i+1] = Id
            rows.append(row)
        rows.append([As[i] for i in range(self.degree)])
        BlockA = bmat(rows)

        rows = []
        for i in range(self.degree-1):
            row = [None] * self.degree
            row[i] = Id
            rows.append(row)
        lastrow = [None] * self.degree
        lastrow[self.degree-1] = -As[self.degree]
        rows.append(lastrow)
        BlockB = bmat(rows)

        return BlockA, BlockB

    def make_resolvents(self):      # called from base constructor

        self._prnt('SpectralProjNGPoly: ' +
                   'Computing resolvents along the contour using')
        defaultinvtype = self.As[0].mat.GetInverseType()
        if self.inverse is None:
            useinvtype = defaultinvtype
        else:
            useinvtype = self.inverse
        self._prnt('                    ' +
                   'inverse type %s' % useinvtype +
                   ' (installed default %s)' % defaultinvtype)
        self.R = []
        sz = self.size()
        for z in self.z:
            self._prnt('SpectralProjNGPoly: ' +
                       'Factorizing %dx%d system at z = %+7.3f% + 7.3fj'
                       % (sz, sz, z.real, z.imag))
            with ng.TaskManager():
                Az = self.polA(z)
                Azinv = Az.Inverse(self.fes.FreeDofs(), inverse=useinvtype)
            self.R.append(Azinv)

    def size(self):
        return int(sum(self.fes.FreeDofs()))

    def check_sym_innerprod(self):  # called from base constructor
        """
        Check only the last block of BB for symmetry
        """
        self._prnt('SpectralProjNGPoly: ' +
                   'Checking if A[d] is nonzero & Hermitian.')
        i, j, bval = self.As[-1].mat.COO()
        b = coo_matrix((bval, (i, j)), dtype=np.complex128)
        normb = norm(b, 'fro')
        if normb < 1e-16:
            raise ValueError('Matrix A[d] is too close to 0')
        # d = norm(b - b.H, 'fro') / normb
        # if d > 1.e-15:
        #     raise ValueError('Matrix A[d] is not Hermitian.')

    def long2list(self, vv):
        """
        Given a long vector "vv" of length degree*ndof, return a list "vs" of
        short vectors such that vs[i] = vv's i-th equally sliced
        part of length ndof.
        """
        vs = []
        for k in range(self.degree):
            start = k*self.fes.ndof
            vs.append(vv[start:start+self.fes.ndof])
        return vs

    def long2block(self, vv):
        return BlockVector(self.long2list(vv))

    def block2long(self, vb, vv):
        """ Copy BlockVector "vb" into long BaseVector "vv" """

        if vb.nblocks != self.degree:
            raise ValueError('Unexpected number of blocks in BlockVector')
        for k in range(vb.nblocks):
            start = k*self.fes.ndof
            vv.FV().NumPy()[start:start+self.fes.ndof] = vb[k]

    def apply_resolvent(self, i, yy, Ryy):
        """
        INPUT: Long BaseVector yy.
        OUTPUT: Long BaseVector Ryy = inv( AA - z * BB ) * BB * yy.
        The result is the same as shiftedinv(i, BB*yy, Ryy).
        """
        suminner = self.applyRscratch[0]
        sumouter = self.applyRscratch[1]
        z = self.z[i]
        ys = self.long2list(yy)
        Rys = self.long2list(Ryy)
        sumouter.FV().NumPy()[:] = 0.0

        for k in range(1, self.degree+1):
            suminner.data = ys[k-1]
            for j in range(1, k):
                suminner.data += complex(z**j) * ys[k-1-j]
            sumouter.data += self.As[k].mat * suminner

        Rys[0].data = self.R[i] * sumouter
        for k in range(1, self.degree):
            Rys[k].data = complex(z) * Rys[k-1] - ys[k-1]

    def shiftedinv(self, i, yy, Ryy):
        """A modification of apply_resolvent(..) method:
        INPUT: Long BaseVector yy.
        OUTPUT: Long BaseVector Ryy = inv( AA - z * BB ) * yy
        """

        suminner = self.applyRscratch[0]
        sumouter = self.applyRscratch[1]
        z = self.z[i]
        d = self.degree
        ys = self.long2list(yy)
        Rys = self.long2list(Ryy)
        sumouter.FV().NumPy()[:] = 0.0

        # k = 0 .. d-1 summands in outer sum
        for k in range(1, d):
            suminner.data = complex(z**(k-1)) * ys[0]
            for j in range(1, k):
                suminner.data += complex(z**(k-1-j)) * ys[j]
            sumouter.data += self.As[k].mat * suminner

        # k = d summand
        suminner.data = complex(z**(d-1)) * ys[0]
        for j in range(1, d-1):
            suminner.data += complex(z**(d-1-j)) * ys[j]
        sumouter.data += self.As[d].mat * suminner - ys[d-1]

        Rys[0].data = self.R[i] * sumouter
        for k in range(1, d):
            Rys[k].data = complex(z) * Rys[k-1] - ys[k-1]

    def shiftedinvadj(self, i, yy, RHyy):
        """
        INPUT: Long BaseVector yy

        OUTPUT: Long BaseVector RHyy = inv(z * BB - AA).H * yy
        """

        ys = self.long2list(yy)
        xs = self.long2list(RHyy)

        zbar = np.conjugate(self.z[i])
        Azinv = self.R[i]
        d = self.degree
        s = self.applyRscratch[1]

        # compute x[d-1] = -inv(P(z).H) * ∑ⱼ₌₀ᵈ⁻¹ conj(z)ʲ y[j]
        s.data = -ys[0]
        zbarj = complex(zbar)
        for j in range(1, d):
            s.data -= zbarj * ys[j]
            zbarj = complex(zbar * zbarj)
        xs[d-1].data = Azinv.H * s

        # compute x[d-2] = -y[d-1] - conj(z) * A[d].H x[d-1] - A[d-1].H x[d-1]
        zbar = complex(zbar)
        s.data = self.As[d].mat.H * xs[d-1]
        s.data *= zbar
        s.data += self.As[d-1].mat.H * xs[d-1]
        xs[d-2].data = -ys[d-1] - s

        # compute x[d-3] ... x[0]
        for j in range(d-3, -1, -1):
            s.data = self.As[j+1].mat.H * xs[d-1]
            xs[j].data = zbar * xs[j+1] - ys[j+1] - s

    def first(self, Y):
        """
        INPUT: NGvec object Y containing long vectors
        OUTPUT: NGvec object y containing short vectors comprising only
        the first blocks (block 0) of vectors of Y.
        """
        y = NGvecs(self.fes, Y.m)
        y._mv[:] = Y._mv[:, :self.fes.ndof]
        return y

    def last(self, Y):
        """
        INPUT: NGvec object Y containing long vectors
        OUTPUT: NGvec object y containing short vectors comprising only
        the first blocks (block 0) of vectors of Y.
        """
        y = NGvecs(self.fes, Y.m)
        lastblock = (self.degree-1)*self.fes.ndof
        y._mv[:] = Y._mv[:, lastblock:]
        return y

    #  Virtual functions needed for FEAST ##########################

    def __mul__(self, v):
        """
        Overwrite v by P * v (the action of spectral projector P on span v),
        where  P = ∮ (zB - A)⁻¹ B  dz  with integral replaced by quadrature.
        """

        if len(v._mv[0]) != self.degree * self.fes.ndof:
            print('*** __mul__: Vector length =', len(v._mv[0]))
            print('*** __mul__: Expected length =', self.degree*self.fes.ndof)
            raise ValueError('Vector length != expected length')

        vv = self.lmulscratch
        Rv = self.longscratch.vec
        d = self.degree
        n = self.fes.ndof

        with ng.TaskManager():

            vv._mv[:v.m] = v._mv

            # BB multiply:   vv[d-1] <-  -A[d] * vv[d-1]
            vv._mv[:v.m, n*(d-1): n*d].data = \
                -self.As[d].mat * v._mv[:, n*(d-1): n*d]

            # NOTE: Above we use new experimental syntax for ng.MultiVectors Y.
            # Y[a:b, p:q] is OPPOSITE of numpy: indices a:b refer to columns
            # and indices p:q refer to rows!!  The above line is equivalent to:
            #   for k in range(v.m):
            #       vv._mv[k][n*(d-1): n*d].data = \
            #           -self.As[d].mat * v._mv[k][n*(d-1): n*d]

            v.zerodata()

            # Sum up  Rv = inv(AA - z * BB) * (modified v) to get Pv
            for i in range(len(self.z)):
                for k in range(v.m):
                    self.shiftedinv(i, vv._mv[k], Rv)
                    v._mv[k].data += complex(self.w[i]) * Rv

        return v

    def __rmul__(self, v):
        """
        Call v * P to overwrite v with Pᴴ v, where
        Pᴴ = ∮ (zB - A)⁻ᴴ Bᴴ  dz  with integral replaced by quadrature.
        """

        if len(v._mv[0]) != self.degree * self.fes.ndof:
            print('*** __rmul__: Vector length =', len(v._mv[0]))
            print('*** __rmul__: Expected length =', self.degree*self.fes.ndof)
            raise ValueError('Vector length != expected length')

        vv = self.rmulscratch
        RHv = self.longscratch.vec
        d = self.degree
        n = self.fes.ndof

        with ng.TaskManager():

            vv._mv[:v.m] = v._mv

            # BB.H multiply:   vv[d-1] <-  -A[d].H * vv[d-1]
            vv._mv[:v.m, n*(d-1): n*d].data = \
                -self.As[d].mat.H * v._mv[:, n*(d-1): n*d]

            v.zerodata()

            # Sum up  RHv = inv(AA - z * BB).H * BB.H * v to get PHv
            for i in range(len(self.z)):
                for k in range(v.m):
                    self.shiftedinvadj(i, vv._mv[k], RHv)
                    v._mv[k].data += complex(np.conjugate(self.w[i])) * RHv
        return v

    def rayleigh(self, q):
        return self.rayleigh_nsa(q, q)

    def rayleigh_nsa(self, ql, qr, qAq=not None, qBq=not None):
        """
        Return qAq[i, j] = (AA qr[j], ql[i])_H and
               qBq[i, j] = (qr[j], ql[i])_H, in the H-inner product,
        unless they are declared None on input. If one is None, only
        the other is computed.

        Cannot use the base class implementation since we've to apply
        block AA and BB.
        """

        qr = qr._mv
        ql = ql._mv
        Aqr = self.rayleighscratch._mv[:len(qr)]
        Bqr = self.rayleighscratch._mv[:len(qr)]

        if qAq is not None:   # compute by MultiVector InnerProduct

            for i in range(len(qr)):
                self.applyAA(qr[i], Aqr[i])
            qAq = ng.InnerProduct(Aqr, ql).NumPy().T

        if qBq is not None:
            for i in range(len(qr)):
                self.applyBB(qr[i], Bqr[i])
            qBq = ng.InnerProduct(Bqr, ql).NumPy().T

        return (qAq, qBq)

    def allocate_feast_scratch_like(self, y):
        """Allocate scratch space used (only) by feast algorithm.
        """
        self.lmulscratch = y.create()
        self.rmulscratch = y.create()
        self.rayleighscratch = y.create()
        self.renormscratch = y._mv[0].CreateVector()

    def residual(self, lam, y):
        """
        Return the inv(A)-norm of residuals A * y[i] - lam[i] * B * y[i]
        """
        raise NotImplementedError()
