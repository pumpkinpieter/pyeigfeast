"""
Module implementing FEAST eigensolver with various discretizations
"""

__all__ = ['SpectralProj',  'SpectralProjMat', 'SpectralProjDPG',
           'Span', 'Vecs', 'NGvecs', 'simple_multiple_zoom',
           'SpectralProjNG', 'SpectralProjNGGeneral',
           'SpectralProjNGR', 'ResolventAB', 'SpectralProjNGPoly']

from .spectralproj import Span, SpectralProj, simple_multiple_zoom
from .spectralproj.mat import Vecs, SpectralProjMat
from .spectralproj.dpg import NGvecs, SpectralProjDPG
from .spectralproj.ngs import SpectralProjNG, SpectralProjNGGeneral
from .spectralproj.ngs import SpectralProjNGR, ResolventAB, SpectralProjNGPoly
