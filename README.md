# PYTHONIC FEAST #

This is a Python3 code repository implementing the FEAST algorithm for computing targeted clusters of eigenvalues of operators. Please see the 
[wiki page](https://bitbucket.org/jayggg/pyeigfeast/wiki/Home) for more information, documentation, and tutorial.

