from pyeigfeast.scripts import eig_dpg_feast_dir_unitsqr
from scipy.linalg import norm


def test_dirichletDPG():
    """ For automated testing """

    lam, Y, history, _ = eig_dpg_feast_dir_unitsqr(0.2, 2, 3, 19, 40, 10,
                                                   inverse=None)
    ewerrors, eferrors, ewhdist, residuals, ews, converged = history
    err = norm(ewerrors[-1], 2)
    success = (err < 0.3) and converged
    assert success, \
        'Obtained larger than expected discretization error %g!' % err


test_dirichletDPG()
