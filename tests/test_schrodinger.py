from ngsolve import CoefficientFunction
from pyeigfeast.scripts import eig_dpg_feast_schrodinger
from math import pi


def test_schrodinger():
    """For automated testing: Use a constant potential and see if you
    recover shifted Dirichlet eigenvalues, ie., ews of -Delta - 10
    should be lambda-10 where lambda is in sepctrum of -Delta.
    """

    V = CoefficientFunction(-10)
    m = 5        # number of vectors in initial eigenspace iterate
    ctr = 10.0    # contour center
    rad = 10       # contour radius
    d = 0.5
    h = 0.3
    p = 2
    npts = 10    # number of points in trapezoidal rule
    lam, Y, mesh, history, P = eig_dpg_feast_schrodinger(h, d, p, V, m,
                                                         ctr, rad, npts)
    print('Exact ew = pi*pi*2 - 10 = ', pi*pi*2-10)
    print('Computed ew = ', lam)
    print('Difference = ', abs((pi*pi*2 - 10) - lam))
    success = abs((pi*pi*2 - 10) - lam) < 0.12
    assert success, \
        'Obtained larger than expected discretization error!'


test_schrodinger()
