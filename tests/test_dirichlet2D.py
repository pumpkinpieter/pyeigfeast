from scipy.linalg import norm
from pyeigfeast.scripts import eig_fem_feast, direct_eig
from netgen.geom2d import unit_square
import ngsolve as ng
import numpy as np

# PARAMETERS:

h = 0.1      # mesh size for finite element discretization
p = 2        # degree of Lagrange finite elements to use
m = 3        # number of vectors in initial eigenspace iterate
ctr = 19.0   # contour center
rad = 40     # contour radius
npts = 8     # number of points in trapezoidal rule


# Function for automatic testing

def test_dirichlet2D():

    ngmesh = unit_square.GenerateMesh(maxh=h)
    mesh = ng.Mesh(ngmesh)
    for i in range(len(mesh.GetBoundaries())):
        # mark Dirichlet bc parts as 'allbdry'
        ngmesh.SetBCName(i, 'allbdry')
    ew, _, _, _, a, b, X = eig_fem_feast(mesh, p, m, ctr, rad, npts,
                                         inverse=None)
    ew = np.sort(ew.real)
    ewscipy = np.sort(direct_eig(a, b, X, k=3).real)
    ll = min(len(ew), len(ewscipy))
    ewerr = norm(ew[:ll]-ewscipy[:ll], 2)
    success = ewerr < 1.e-10
    errmsg = 'FEAST eigenvalues do not match SCIPY eigenvalues!'

    assert success, errmsg


test_dirichlet2D()
