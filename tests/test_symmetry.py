"""
Test if the finite element spectral projector is symmetric
"""

from netgen.geom2d import unit_square
from ngsolve import grad, dx
import ngsolve as ng
import numpy as np
from pyeigfeast import SpectralProjNG
from scipy.sparse import coo_matrix

ng.ngsglobals.msg_level = 1

ngm0 = unit_square.GenerateMesh(maxh=0.49)
mesh0 = ng.Mesh(ngm0)
for i, bdries in enumerate(mesh0.GetBoundaries()):
    ngm0.SetBCName(i, 'allbdry')
ng.Draw(mesh0)

X = ng.H1(mesh0, order=2, dirichlet='allbdry', complex=True)
u, v = X.TnT()
a = ng.BilinearForm(X)
a += grad(u) * grad(v) * dx
b = ng.BilinearForm(X)
b += u * v * dx
a.Assemble()
b.Assemble()

free0 = X.FreeDofs()
free0 = np.where(free0)[0]


def mat(ngmat):
    i, j, v = ngmat.COO()
    M1 = coo_matrix((v, (i, j)), dtype=np.complex128)
    MM1 = M1.todense()
    MM1 = MM1[:, free0]
    MM1 = MM1[free0, :]
    return np.array(MM1)


c = 19.0  # integration contour center
r = 40    # integration contour radius
n = 4     # number of points in trapezoidal rule

P1 = SpectralProjNG(X, a.mat, b.mat, radius=r,
                    center=c, npts=n, reduce_sym=True)

M = mat(b.mat)
A = mat(a.mat)

S1 = P1.matrix_filter()


def test_selfadj():

    symdiffM = np.linalg.norm(M @ S1 - S1.T.conjugate() @ M)
    symdiffA = np.linalg.norm(A @ S1 - S1.T.conjugate() @ A)

    print('Test of selfadjointness in L^2:   diff = ', symdiffM)
    print('Test of selfadjointness in H_0^1: diff = ', symdiffA)

    successA = symdiffA < 1.e-14
    successM = symdiffM < 1.e-14
    assert successA, 'SpectralProj(Laplacian) found not selfadjoint' + \
        'in A-innerproduct. \n(Check symmetry of your quadrature rule?)'
    assert successM, 'SpectralProj(Laplacian) found not selfadjoint' + \
        'in M-inner product. \n(Check symmetry of your quadrature rule?)'


test_selfadj()
