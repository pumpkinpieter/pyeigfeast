"""
Test if NGSolve's InnerProduct on Multivectors is doing what we assume it to do
"""

import ngsolve as ng
import numpy as np


def test_innerprod_multivec():

    n = 5  # len of vectors
    m = 3  # num multivectors

    VV = np.random.rand(n, m) + 1j * np.random.rand(n, m)
    WW = np.random.rand(n, m) + 1j * np.random.rand(n, m)

    z = ng.CreateVVector(n, complex=True)
    mV = ng.MultiVector(z, m)
    mW = ng.MultiVector(z, m)

    for i in range(m):
        mV[i].FV().NumPy()[:] = VV[:, i]
        mW[i].FV().NumPy()[:] = WW[:, i]

    # # NGSolve behavior before Mar 5, 2023:
    # #
    # #    InnerProduct(V, W) = transpose of W.H @ V
    # diff = ng.InnerProduct(mV, mW).NumPy() - (WW.conj().T @ VV).T
    # ndf = np.linalg.norm(diff)
    # print('|| InnerProduct(V, W) - Transpose(W.H @ V) || = ', ndf)
    # assert ndf < 1e-15, 'NGSolve InnerProduct does not work as expected!'

    # NGSolve behavior proposed change after Mar 5, 2023:
    #
    #    InnerProduct(V, W) =  W.H @ V
    diff = ng.InnerProduct(mV, mW).NumPy() - (WW.conj().T @ VV)
    ndf = np.linalg.norm(diff)
    print('|| InnerProduct(V, W) - W.H @ V || = ', ndf)
    assert ndf < 1e-14, 'NGSolve InnerProduct does not work as expected!'


test_innerprod_multivec()
