"""
Test the adjoint of spectral projector for correctness on random vec
"""

from netgen.geom2d import unit_square
from ngsolve import grad, dx
import ngsolve as ng
from pyeigfeast import SpectralProjNG, NGvecs, SpectralProjNGGeneral


ng.ngsglobals.msg_level = 1
inversetype = 'umfpack'

ngm0 = unit_square.GenerateMesh(maxh=0.49)
mesh0 = ng.Mesh(ngm0)
for i, bdries in enumerate(mesh0.GetBoundaries()):
    ngm0.SetBCName(i, 'allbdry')
ng.Draw(mesh0)

X = ng.H1(mesh0, order=2, dirichlet='allbdry', complex=True)
u, v = X.TnT()
a = ng.BilinearForm(X)
a += (1+1j) * grad(u) * grad(v) * dx
a.Assemble()


def test_adjoint_spectralprojng():
    """
    SpectralProjNG adjoint is calculated wrt the inner product generated
    by b.mat. This test verifies this.
    """

    print('\nTesting SpectralProjNG adjoint' + '='*27)
    b = ng.BilinearForm(X)
    b += u * v * dx
    b.Assemble()

    P = SpectralProjNG(X, a.mat, b.mat, radius=19, center=40, npts=4,
                       reduce_sym=False, inverse=inversetype)
    Y = NGvecs(X, 1)
    Y.setrandom(seed=1)

    W = NGvecs(X, 1)
    W.setrandom(seed=1)

    PY = P * Y   # spectral proj action
    WP = W * P   # adjoint action

    # Compute (P * Y, W)_B in the B-inner product
    bPY = PY._mv[0].CreateVector()
    bPY.data = b.mat * PY._mv[0]
    ipPYW = ng.InnerProduct(bPY, W._mv[0])

    # Compute (Y, adjP * W)_B in the B-inner product
    bY = bPY.CreateVector()
    bY.data = b.mat * Y._mv[0]
    ipYPW = ng.InnerProduct(bY, WP._mv[0])
    print('ipPYW: ', ipPYW)
    print('ipYPW: ', ipYPW)

    assert abs(ipPYW - ipYPW) < 1e-15, \
        'SpectralProjNG adjoint is incorrect'


def test_adjoint_spectralprojnggen():

    print('\nTesting SpectralProjNGGeneral adjoint' + '='*20)
    b = ng.BilinearForm(X)
    b += (0.1 + 10j) * u * v * dx
    b.Assemble()

    P = SpectralProjNGGeneral(X, a.mat, b.mat, radius=19, center=40,
                              npts=4, inverse=inversetype)
    Y = NGvecs(X, 1)
    Y.setrandom(seed=1)

    W = NGvecs(X, 1)
    W.setrandom(seed=1)

    PY = P * Y   # spectral proj action
    WP = W * P   # adjoint action

    # Compute (P * Y, W) in the Euclidean inner product
    ipPYW = ng.InnerProduct(PY._mv[0], W._mv[0])

    # Compute (Y, adjP * W) in the Euclidean inner product
    ipYPW = ng.InnerProduct(Y._mv[0], WP._mv[0])

    print('ipPYW: ', ipPYW)
    print('ipYPW: ', ipYPW)

    assert abs(ipPYW - ipYPW) < 1e-15, \
        'SpectralProjNGGeneralized adjoint is incorrect'


test_adjoint_spectralprojng()
test_adjoint_spectralprojnggen()
