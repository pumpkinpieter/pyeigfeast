from pyeigfeast.scripts import run_feast_1dtridiag


def test_dirichlet1D():

    m = 5                     # initial number of vectors
    numtrap = 20              # number of trapezoidal rule points
    n = 100                   # mesh size is h = 1 / (n+1)
    ctr = 0.50                 # contour center
    rad = 0.01                 # contour radius
    lam, Y, history, exew, ewerr, _ \
        = run_feast_1dtridiag(n, m, ctr, rad, numtrap)

    success = ewerr < 1.e-12
    errmsg = '1D Dirichlet eigenvalues incorrect!'
    assert success, errmsg


test_dirichlet1D()
