from pyeigfeast.spectralproj.ngs import NGvecs
from netgen.geom2d import unit_square
import ngsolve as ng
import numpy as np


def test_np_import():

    mesh = ng.Mesh(unit_square.GenerateMesh(maxh=0.1))
    X = ng.H1(mesh, order=3, complex=True)
    m = 10
    Yy = NGvecs(X, m)
    Yy.setrandom()
    y = Yy.tonumpy()  # y should be random with 0 bc

    # check if fromnumpy followed tonumpy gives the same
    Y = NGvecs(X, m)
    Y.fromnumpy(y)
    yy = Y.tonumpy()

    assert np.linalg.norm(y-yy, 'fro') < 1e-15, \
        'fromnumpy() -> tonumpy() does not give consistent results'

    Y.setrandom()
    yy = Y.tonumpy()
    Yy.fromnumpy(yy)
    for i in range(m):
        Y._mv[i].data -= Yy._mv[i]
        assert max(np.array(Y._mv[i])) < 1.e-15, \
            'tonumpy() -> fromnumpy() does not give consistent results'


test_np_import()


def test_iterate_and_index_grid_functions():
    mesh = ng.Mesh(unit_square.GenerateMesh(maxh=0.1))
    X = ng.H1(mesh, order=3, complex=True)
    m = 10
    Yy = NGvecs(X, m)
    Yy.setrandom()
    for y in Yy:
        assert type(y) == ng.GridFunction
    y = Yy[8]
    Yy[7] = y.vec
    assert type(y) == ng.GridFunction
    assert abs(Yy[7].vec[3] - Yy[8].vec[3]) < 1.0e-15
